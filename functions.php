<?php

define('ENV', 'production');
define('DOMAIN', 'arcadia');
define('BASE', dirname(__FILE__));
define('THEME_IMG_PATH', get_stylesheet_directory_uri() . '/images');
define('GOOGLEAPI', 'AIzaSyDivzhX9fUCCqvaJWhbVXu-y1EDBCycwuU');

require_once(BASE . '/inc/helpers.php');
require_once(BASE . '/inc/autoloader.php');

use MaxMind\Db\Reader;

require_once(WP_PLUGIN_DIR . '/woocommerce-points-and-rewards/includes/class-wc-points-rewards-product.php');

\Arcadia\Setup::init();
\Arcadia\Woocommerce::init();

function theme_setup()
{
    // Thumbnails
    add_theme_support('post-thumbnails');

    // Title Tag
    add_theme_support('title-tag');

    // Menus
    register_nav_menu('primary', __('Navigation Menu', DOMAIN));
    register_nav_menu('footer1', __('Footer Column 1', DOMAIN));
    register_nav_menu('footer2', __('Footer Column 2', DOMAIN));
    register_nav_menu('footer3', __('Footer Column 3', DOMAIN));
    register_nav_menu('mobile', __('Mobile Menu (Always)', DOMAIN));
    register_nav_menu('mobileham', __('Mobile Menu (Hamburger)', DOMAIN));

    // When inserting an image don't link it
    update_option('image_default_link_type', 'none');

    // Remove Gallery Styling
    add_filter('use_default_gallery_style', '__return_false');

    // Disable Gutenburg
    add_filter('use_block_editor_for_post_type', '__return_false', 10);

    // Additional Image Sizes
    add_image_size('banner', 1920, 1920);

    // Woocommerce
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
}

add_action('after_setup_theme', 'theme_setup');

function requeueGiftCards()
{
    $dependencies = ['jquery', 'jquery-ui-datepicker'];
    $dependencies = apply_filters('woocommerce_gc_script_dependencies', $dependencies);

    wp_register_script('rv-gc-main', get_template_directory_uri() . '/js/giftcards.js', $dependencies, WC_GC()->get_plugin_version(), true);

    $params = apply_filters('woocommerce_gc_front_end_params', array(
        'version'                           => WC_GC()->get_plugin_version(),
        'wc_ajax_url'                       => WC_AJAX::get_endpoint('%%endpoint%%'),
        'gmt_offset'                        => -1 * wc_gc_get_gmt_offset(), // Revert value to match JS.
        'date_input_timezone_reference'     => wc_gc_get_date_input_timezone_reference(),
        'security_redeem_card_nonce'        => wp_create_nonce('redeem-card'),
        'security_remove_card_nonce'        => wp_create_nonce('remove-card'),
        'security_update_use_balance_nonce' => wp_create_nonce('update-use-balance')
    ));

    wp_localize_script('rv-gc-main', 'wc_gc_params', $params);
    wp_enqueue_script('rv-gc-main');
}

/**
 * Load theme specific assets
 */
function scripts_styles()
{
    requeueGiftCards();

    wp_enqueue_style(DOMAIN . '-style', get_stylesheet_uri() . '?20211214');

    wp_enqueue_script(DOMAIN . '-polyfill', 'https://cdn.polyfill.io/v2/polyfill.min.js', [], null);
    wp_enqueue_script(DOMAIN . '-slick', get_template_directory_uri() . '/js/slick.min.js', [], null, true);
    wp_enqueue_script(DOMAIN . '-main', get_template_directory_uri() . '/js/main.js?20211029', [DOMAIN . '-slick'], null, true);

    if (is_page('about-us')) {
        wp_enqueue_script(DOMAIN . '-meet2', get_template_directory_uri() . '/js/meet2.js', [], null, true);
        wp_enqueue_style('meet2css', get_template_directory_uri() . '/mystyle.css');
    }

    if (is_product() && strcasecmp($_SERVER['SERVER_NAME'], 'www.revolution-nutrition.com') == 0) {
            wp_enqueue_script(DOMAIN . '-payBright', 'https://app.paybright.com/dist/sdk.js?public_key=qKgFZyX248g8YN7Ehj3wdBvPPPDax1eDDACTnWsfum4J5E1AGD', [], null, true);
            wp_enqueue_script(DOMAIN . '-payBrightCustom', get_template_directory_uri() . '/js/payBrightCustom.js?20211208', [], null, true);
    }
}

add_action('wp_enqueue_scripts', 'scripts_styles', 999);

if (isset($_GET['added']) && $_GET['added'] != '') {
    add_action('wp_enqueue_scripts', 'snap_pixel_event_add_to_cart', 999);
}

function get_variation_upc($parent, $variation_id)
{
    $variations = $parent->get_available_variations();

    foreach ($variations as $key => $variation) {
        if ($variation['variation_id'] == $variation_id) {
            return $variation['upc_id'];
        }
    }

    return null;
}

function snap_pixel_event_add_to_cart()
{
    try {
        $variation_id = $_GET['added'];
        $variation_product = wc_get_product($variation_id);
        $parent = $variation_product->parent;
        $upc_id = get_variation_upc($parent, $variation_id);
        $product_id = $variation_product->parent_id;
        $term_list = wp_get_post_terms($product_id, 'product_cat', array(
            'fields' => 'ids',
        ));
        $cat_id = (int) $term_list[0];
        $category = get_term($cat_id, 'product_cat');
        $product_category = $category->name;
        $product_price = $variation_product->price;
        $product_currency = get_woocommerce_currency();
        $snap_pixel_code = get_option('snap_pixel_code');//pixel_id
        $current_user = wp_get_current_user();
        $email=$current_user->user_email;
        $pixel_id=$snap_pixel_code['pixel_id'];
    } catch (\Throwable $e) {
        $pixel_id="";
        $email="";
        $product_currency="CA";
        $product_price="";
        $product_category="";
        $variation_id="";
    }
    ?>
    <script type="text/javascript">
        (function(win, doc, sdk_url){
            if(win.snaptr) return;
            var tr=win.snaptr=function(){
            tr.handleRequest? tr.handleRequest.apply(tr, arguments):tr.queue.push(arguments);
        };
            tr.queue = [];
            var s='script';
            var new_script_section=doc.createElement(s);
            new_script_section.async=!0;
            new_script_section.src=sdk_url;
            var insert_pos=doc.getElementsByTagName(s)[0];
            insert_pos.parentNode.insertBefore(new_script_section, insert_pos);
        })(window, document, 'https://sc-static.net/scevent.min.js');

            snaptr('init','<?php echo $pixel_id ?>',{
            'user_email':'<?php echo  $email ?>'
        })
        snaptr('track', 'ADD_CART', {
            'currency': '<?php echo $product_currency ?>' ,
            'price': '<?php echo $product_price ?>' ,
            'item_category': '<?php echo $product_category?>' ,
            'item_ids':  '<?php echo $upc_id ?>' });
    </script>
    <?php
}
/**
 * Custom Editor Styles
 * @param  array $init TinyMCE Options
 * @return array       Updated TinyMCE Options
 */
function editor_style_options($init)
{
    $style_formats = [
        [
            'title' => 'Small Text',
            'selector' => '*',
            'classes' => 'text-small',
        ],
        [
            'title' => 'Medium Text',
            'selector' => '*',
            'classes' => 'text-medium',
        ],
        [
            'title' => 'Large Text',
            'selector' => '*',
            'classes' => 'text-large',
        ],
        [
            'title' => 'Extra Large Text',
            'selector' => '*',
            'classes' => 'text-xlarge',
        ],
        [
            'title' => 'Button',
            'selector' => 'a',
            'classes' => 'btn',
        ],
    ];

    $init['style_formats'] = json_encode($style_formats);

    return $init;
}

add_filter('tiny_mce_before_init', 'editor_style_options');

/**
 * ACF Maps Key
 */
function my_acf_init()
{
    acf_update_setting('google_api_key', GOOGLEAPI);
}

add_action('acf/init', 'my_acf_init');

function my_wp_nav_menu_objects($items, $args)
{
    foreach ($items as &$item) {
        $megamenu = get_field('activate_mega', $item);
        $columns = get_field('mega_columns', $item);
        $fixed = get_field('mega_fixed_first', $item);

        if ($megamenu) {
            $item->classes[] = 'mega-menu';
            $item->classes[] = 'mega-col-' . $columns;
        }

        if ($fixed) {
            $item->classes[] = 'mega-fixed';
        }
    }

    return $items;
}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function plugin_tidy_up()
{
    if (is_admin()) {
        return;
    }

    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');
    wp_dequeue_style('wc-block-style');
    wp_dequeue_style('dashicons');

    // Free Gifts
    wp_dequeue_style('owl-carousel');
    wp_dequeue_style('fgf-owl-carousel');
    wp_dequeue_style('lightcase');
    wp_dequeue_style('fgf-frontend-css');
    wp_dequeue_style('fgf-inline-style');

    wp_dequeue_script('owl-carousel');
    wp_dequeue_script('fgf-owl-carousel');
    wp_dequeue_script('lightcase');
    wp_dequeue_script('fgf-lightcase');
    wp_dequeue_script('fgf-frontend');

    if (!is_checkout() && !is_wc_endpoint_url('add-payment-method')) {
        // Moneris
        wp_dequeue_style('sv-wc-payment-gateway-payment-form');
        wp_dequeue_style('wc-moneris-hosted-tokenization');

        wp_dequeue_script('sv-wc-payment-gateway-payment-form');
        wp_dequeue_script('wc-moneris-hosted-tokenization');
    }

    if (!is_checkout() && !is_cart()) {
        // Canada Post (Cart & Checkout)
        wp_dequeue_script('ph-canada-post-front-end');
    }

    if (!is_singular('product') && !is_product_category() && !is_search()) {
        // Variations
        wp_dequeue_style('woo-variation-swatches');
        wp_dequeue_style('woo-variation-swatches-inline');
        wp_dequeue_style('woo-variation-swatches-theme-override');
        wp_dequeue_style('woo-variation-swatches-tooltip');
        wp_dequeue_style('woo-variation-swatches-tooltip-inline');
        wp_dequeue_style('woo-variation-swatches-pro');
        wp_dequeue_style('woo-variation-swatches-pro-inline');
        wp_dequeue_style('woo-variation-swatches-pro-theme-override');

        wp_dequeue_script('woo-variation-swatches');
        wp_dequeue_script('woo-variation-swatches-pro');
    }

    if (!is_singular('product')) {
        // Discount Rules
        wp_dequeue_style('woo_discount_rules-customize-table-ui-css');
        wp_dequeue_style('woo_discount_pro_style');

        wp_dequeue_script('awdr-main');
        wp_dequeue_script('awdr-dynamic-price');
        wp_dequeue_script('woo_discount_pro_script');
    }
}

add_action('wp_enqueue_scripts', 'plugin_tidy_up', 999);

function check_for_product($status)
{
    if (is_admin()) {
        return $status;
    }

    if (!is_singular('product') && !is_product_category() && !is_search()) {
        return true;
    }
}

add_filter('disable_wvs_body_class', 'check_for_product');
add_filter('disable_wvs_enqueue_scripts', 'check_for_product');
add_filter('disable_wvs_inline_style', 'check_for_product');

add_filter('comments_template_query_args', function ($args) {
    if (get_field('review_feed')) {
        $args['post_id'] = get_field('review_feed')->ID;
    }

    return $args;
}, 999);

/**
 * Shortcodes
 */
add_shortcode('logged_in', function ($attributes, $content) {
    if (is_user_logged_in()) {
        return apply_filters('the_content', $content);
    }

    return '';
});

add_shortcode('not_logged_in', function ($attributes, $content) {
    if (!is_user_logged_in()) {
        return apply_filters('the_content', $content);
    }

    return '';
});

add_filter('xmlrpc_enabled', '__return_false', PHP_INT_MAX);
add_filter('xmlrpc_methods', '__return_empty_array', PHP_INT_MAX);

function remove_checkout_optional_fields_label($field, $key, $args, $value)
{
    if (is_checkout() && ! is_wc_endpoint_url()) {
        $optional = '&nbsp;<span class="optional">(' . esc_html__('optional', 'woocommerce') . ')</span>';
        $field = str_replace($optional, '', $field);
    }

    return $field;
}

add_filter('woocommerce_form_field', 'remove_checkout_optional_fields_label', 10, 4);

function add_subscribe_checkbox_to_registration()
{
    $content = '<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">';
    $content .= '<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="woocommerce_newsletter" type="checkbox" name="subscribe_newsletter" value="1" checked="checked">';
    $content .= '<label for="woocommerce_newsletter" class="woocommerce-form__label woocommerce-form__label-for-checkbox inline" style="line-height: 1.4;">I want to be the first to know about new products, new flavours and great deals through the Revolution Nutrition newsletter.</label>';
    $content .= '</p>';

    echo $content;
}

add_action('woocommerce_register_form', 'add_subscribe_checkbox_to_registration');

function add_subscriber_to_klaviyo($customer_id, $new_customer_data, $password_generated)
{
    if (!isset($_POST['subscribe_newsletter'])) {
        return;
    }

    $email = $new_customer_data['user_email'];
    $list = 'ULWSKV';

    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => 'https://a.klaviyo.com/api/v2/list/' . $list . '/members?api_key=pk_812d6e125ae7c5ae2e94e9c8e358926572',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode([
            'profiles' => [
                ['email' => $email],
            ],
        ]),
        CURLOPT_HTTPHEADER => [
            "Accept: application/json",
            "Content-Type: application/json"
        ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
}

add_action('woocommerce_created_customer', 'add_subscriber_to_klaviyo', 10, 3);

/**
 * @return string Country code like 'CA', 'US', ..., or 'unknown' if country is not found in GEO ip database.
 */
function find_country_based_on_remote_ip($remoteIp = null): string
{
    $geodb = new Reader(get_template_directory().'/inc/Geoip/GeoIP2-Country.mmdb');
    $found = $geodb->get($remoteIp);
    $country = 'unknown';

    if ($found !== null) {
        /*
         * Base on Mixmind's document:
         * The country is the country where the IP address is located.
         * The registered_country is the country in which the IP is registered.
         * These two may differ in some cases.
         * So looking for country first. If it is null then check registered_country.
         */
        if (!is_null($found['country']['iso_code'])) {
            $country = $found['country']['iso_code'];
        } else {
            $country = $found['registered_country']['iso_code'];
        }
    }

    return $country;
}

add_filter('body_class', function ($classes) {
    $serverName = $_SERVER['SERVER_NAME'];
    if (strcasecmp($serverName, 'www.revolution-nutrition.com') == 0) {
        return array_merge($classes, array('class-ca'));
    } elseif (strcasecmp($serverName, 'www.us.revolution-nutrition.com') == 0) {
        return array_merge($classes, array('class-us'));
    }
    return $classes;
});


function pb_code($fragments)
{
    $serverName = $_SERVER['SERVER_NAME'];

    if (! strcasecmp($serverName, 'www.revolution-nutrition.com') == 0) {
        return $fragments;
    }

    global $woocommerce;
    ob_start(); ?>
    <div class="cart-contents">
    <?php
    // $pb_cart_price = $woocommerce->cart->get_cart_contents_total() + $woocommerce->cart->get_cart_contents_tax();
    //  $pb_cart_price = apply_filters('woocommerce_cart_contents_total', $woocommerce->cart->get_cart_contents_total() + $woocommerce->cart->get_cart_contents_tax());
    $pb_cart_price = WC()->cart->total;
    ?>
    <script id='pb_prequalify' type='text/javascript' src='https://app.paybright.com/dist/sdk.js?public_key=qKgFZyX248g8YN7Ehj3wdBvPPPDax1eDDACTnWsfum4J5E1AGD&financedamount=$<?php echo $pb_cart_price ?>'></script>
    <script>
    setTimeout(function () {
    pb_prequalify_init({
        triggerElement: "link",
        triggerClass: "far fa-info-circle",
        triggerText:"  ",
        lang: "en",
        hideTriggerLeadText: false,
        noFiguresInModal: false,
        showDecimals: true,
    });
    }, 250);
    </script>
    <div id='paybright-widget-container'></div>
    </div>
    <?php
    $fragments['div.cart-contents'] = ob_get_clean();

    return $fragments;
}


add_action('woocommerce_add_to_cart_fragments', 'pb_code');
