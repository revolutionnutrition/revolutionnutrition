<?php

/**
 * Decide on text color based on background
 * @param  string $hexcolor Background Color
 * @param  string $dark     Dark contrast colour
 * @param  string $light    Light contrast colour
 * @return string           Color
 */
function get_contrast($hexcolor, $dark = 'black', $light = 'white')
{
    if (substr($hexcolor, 0, 1) == '#') {
        $hexcolor = substr($hexcolor, 1);
    }

    $r = hexdec(substr($hexcolor, 0, 2));
    $g = hexdec(substr($hexcolor, 2, 2));
    $b = hexdec(substr($hexcolor, 4, 2));

    $yiq = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;

    return ($yiq >= 128) ? $dark : $light;
}

/**
 * Retrieve meta data about attachments
 * @param  integer $attachment_id Attachment ID
 * @param  array   $size          Image Size
 * @return array                  Details
 */
function wp_get_attachment($attachment_id, $size = [])
{
    $src        = wp_get_attachment_image_src($attachment_id, $size);
    $attachment = get_post($attachment_id);

    // Attachment doesn't exist
    if (!$src) {
        return false;
    }

    return [
        'alt'         => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
        'caption'     => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href'        => get_permalink($attachment->ID),
        'src'         => $src[0],
        'width'       => $src[1],
        'height'      => $src[2],
        'title'       => $attachment->post_title
    ];
}

/**
 * Make sure HTTP has been added to URL
 * @param  string $url URL
 * @return string      Fixed URL
 */
function check_url($url)
{
    if (substr($url, 0, 1) != '#' && substr($url, 0, 4) != 'http') {
        return 'http://' . $url;
    }

    return $url;
}

/**
 * Get menu name by theme location
 * @param  string $location Location slug
 * @return string           Menu name
 */
function get_menu_name_from_location($location)
{
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations[$location]);

    return $menu->name;
}

/**
 * Array map with preserved keys
 * @param  function $callback Callback function
 * @param  array    $array    Array to iterate over
 * @return array              Newly mapped array
 */
function array_map_with_keys($callback, $array)
{
    $keys = array_keys($array);

    return array_combine($keys, array_map($callback, $keys, $array));
}

function wc_checkout_shipping_html()
{
    $packages = WC()->shipping()->get_packages();
    $first = true;

    foreach ($packages as $i => $package) {
        $chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
        $product_names = array();

        if (count($packages) > 1) {
            foreach ($package['contents'] as $item_id => $values) {
                $product_names[ $item_id ] = $values['data']->get_name() . ' &times;' . $values['quantity'];
            }

            $product_names = apply_filters('woocommerce_shipping_package_details_array', $product_names, $package);
        }

        wc_get_template(
            'checkout/checkout-shipping.php',
            array(
                'package'                  => $package,
                'available_methods'        => $package['rates'],
                'show_package_details'     => count($packages) > 1,
                'show_shipping_calculator' => is_cart() && apply_filters('woocommerce_shipping_show_shipping_calculator', $first, $i, $package),
                'package_details'          => implode(', ', $product_names),
                'package_name'             => apply_filters('woocommerce_shipping_package_name', (($i + 1) > 1) ? sprintf(_x('Shipping %d', 'shipping packages', 'woocommerce'), ($i + 1)) : _x('Shipping', 'shipping packages', 'woocommerce'), $i, $package),
                'index'                    => $i,
                'chosen_method'            => $chosen_method,
                'formatted_destination'    => WC()->countries->get_formatted_address($package['destination'], ', '),
                'has_calculated_shipping'  => WC()->customer->has_calculated_shipping(),
            )
        );

        $first = false;
    }
}

function check_bulk_category_id($cat_id_list, $bulk_id)
{
    if (count($cat_id_list) > 0) {
        foreach ($cat_id_list as $key => $value) {
            if ($bulk_id == $value) {
                return true;
            }
        }
    }
    return false;
}

function removeBulk(&$cross_sells, $item_num, $bulk_id)
{
    if ($item_num > 0) {
        for ($i = 0; $i < $item_num; $i++) {
            foreach ($cross_sells as $key => &$sell) {
                $product = wc_get_product($sell->id);
                $sell_cat_id_list = $product->get_category_ids();
                if (check_bulk_category_id($sell_cat_id_list, $bulk_id)) {
                    unset($cross_sells[$key]);
                    break;
                }
            }
        }
    }
}

function removeRnSells(&$cross_sells, $item_num, $bulk_id)
{
    if ($item_num > 0) {
        for ($i = 0; $i < $item_num; $i++) {
            foreach ($cross_sells as $key => &$sell) {
                $product = wc_get_product($sell->id);
                $sell_cat_id_list = $product->get_category_ids();
                if (!check_bulk_category_id($sell_cat_id_list, $bulk_id)) {
                    unset($cross_sells[$key]);
                    break;
                }
            }
        }
    }
}

function get_category_id($category_name)
{
    $term = get_terms([
        'taxonomy' => 'product_cat',
        'name' => $category_name,
    ]);
    if ($term[0]) {
        return $term[0]->term_id;
    }
}

function cal_count_cart_bulk($cart_products, &$count_cart_bulk, $bulk_id)
{
    foreach ($cart_products as $key => &$product) {
        $product_type = new WC_Product($product['product_id']);
        $cart_cat_id_list = $product_type->get_category_ids();
        if (check_bulk_category_id($cart_cat_id_list, $bulk_id)) {
            $count_cart_bulk++;
        }
    }
}

function cal_sell_rn_bulk($cross_sells, &$count_sell_bulk, &$count_sell_rn, $bulk_id)
{
    foreach ($cross_sells as $key => &$sell) {
        $product = wc_get_product($sell->id);
        $sell_cat_id_list = $product->get_category_ids();
        if (check_bulk_category_id($sell_cat_id_list, $bulk_id)) {
            $count_sell_bulk++;
        } else {
            $count_sell_rn++;
        }
    }
}

function getRandomNum(&$temp, $rn_length)
{
    do {
        $random_rn = rand(0, $rn_length - 1);
    } while (in_array($random_rn, $temp));
    array_push($temp, $random_rn);
    return $random_rn;
}

function verify_exist_in_cart($postid)
{
    $cart_products = WC()->cart->get_cart();
    foreach ($cart_products as $key => $product) {
        if ($product['product_id'] == $postid) {
            return true;
        }
    }
    return false;
}

function verify_exist_in_cross_sells($cross_sells, $postid)
{
    foreach ($cross_sells as $key => &$sell) {
        if ($sell->id == $postid) {
            return true;
        }
    }
    return false;
}

function setRnSells(&$cross_sells, $item_num)
{
    $revolution_post = getProducts('Customer Also Bought');
    $rn_product = $revolution_post->posts;
    $rn_length = count($rn_product);
    $temp = [];
    if ($rn_length > 0 && $item_num > 0) {
        for ($i = 0; $i < $item_num; $i++) {
            //$random_rn = rand(0, $rn_length - 1);
            $random_rn = getRandomNum($temp, $rn_length);
            $postid = $rn_product[$random_rn]->ID;
            if (!verify_exist_in_cart($postid) && !verify_exist_in_cross_sells($cross_sells, $postid)) {
                $product = new WC_Product($postid);
                array_push($cross_sells, $product);
            } else {
                $i--;
            }
        }
    }
}

function setBulkSells(&$cross_sells, $total_item_num)
{
    $bulk_post = getProducts('Bulk');
    $bulk_product = $bulk_post->posts;
    $bulk_length = count($bulk_product);
    $temp = [];
    if ($total_item_num > 0 && $bulk_length > 0) {
        for ($i = 0; $i < $total_item_num; $i++) {
            $random_bu = getRandomNum($temp, $bulk_length);
            $b_postid = $bulk_product[$random_bu]->ID;
            if (!verify_exist_in_cart($b_postid) && !verify_exist_in_cross_sells($cross_sells, $b_postid)) {
                $b_product = new WC_Product($b_postid);
                array_push($cross_sells, $b_product);
            } else {
                $i--;
            }
        }
    }
}

function getProducts($category_name)
{
    $args_bulk = [
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => -1,
        'tax_query'             => [
            [
                'taxonomy'      => 'product_cat',
                'field'         => 'term_id',
                'terms'         => get_category_id($category_name),
                'operator'      => 'IN'
            ],
            [
                'taxonomy'      => 'product_visibility',
                'field'         => 'slug',
                'terms'         => 'exclude-from-catalog',
                'operator'      => 'NOT IN'
            ]
        ]
    ];
    return new WP_Query($args_bulk);
}
