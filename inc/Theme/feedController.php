<?php

namespace Arcadia\Theme;

use WP_Query;
use WC_Product_Variable;
use SimpleXMLElement;
use WC_Product_Variation;

class feedController
{
    public function index()
    {
        self::getAllReviewsFeeds(self::getPost());
    }

    public static function getPost()
    {
        $args = [
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        ];

        return new WP_Query($args);
    }

    public static function getProductComments($product_id)
    {
        $args = [
            'post_id' => $product_id,
            'status' => 'approve',
            'post_status' => 'publish',
            'post_type' => 'product',
        ];

        return get_comments($args);
    }

    public static function getAllReviewsFeeds($products_post)
    {
        $feed = new SimpleXMLElement('<feed xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xsi:noNamespaceSchemaLocation="http://www.google.com/shopping/reviews/schema/product/2.3/product_reviews.xsd" />');
        $feed->addChild('version', "2.3");
        $aggregator = $feed->addChild('aggregator');
        $aggregator->addChild('name', "Revolution Nutrition Reviews Aggregator");

        $publisher = $feed->addChild('publisher');
        $publisher->addChild('name', "Revolution Nutrition");
        $publisher->addChild('favicon', "https://www.revolution-nutrition.com/wp-content/themes/revolution/img/logo.png");
        $sub_brandId_list = [293, 652, 653];
        $reviews = $feed->addChild('reviews');

        while ($products_post->have_posts()) {
            $products_post->the_post();
            $product_post = $products_post->post;
            $product = wc_get_product($product_post->ID);
            $cat_id_list = $product->get_category_ids();
            $sub_brandId = array_values(array_intersect($cat_id_list, $sub_brandId_list));
            $sub_brand_name = empty($sub_brandId) ? "Revolution Nutrition" : get_term($sub_brandId[0])->name;
            $comments = self::getProductComments($product->id);

            if (count($comments) == 0) {
                continue;
            }

            $variations = $product->get_available_variations();

            foreach ($comments as $key => $comment) {
                $products_v = self::getReviewFeed($comment, $reviews);
                self::getProductsFeed($variations, $products_v, $product, $sub_brand_name);
            }
        }

        Header('Content-type: text/xml');
        print($feed->asXML());
    }

    public static function getReviewFeed($comment, $reviews)
    {
        $rating = get_comment_meta($comment->comment_ID, 'rating', true);
        $product_link = get_permalink();
        $review = $reviews->addChild('review');
        $review->addChild('review_id', $comment->comment_ID);
        $reviewer = $review->addChild('reviewer');
        $reviewer_name = explode(" ", $comment->comment_author);
        $reviewer->addChild('name', $reviewer_name[0]);
        $timestamp = date("Y-m-d\TH:i:s\Z", strtotime($comment->comment_date));
        $review->addChild('review_timestamp', $timestamp);
        $content = str_replace("&amp;", "and", $comment->comment_content);
        $review->addChild('content', $content);
        $review_url = $review->addChild('review_url ', $product_link);
        $review_url->addAttribute('type', 'singleton');
        $ratings = $review->addChild('ratings');
        $overall = $ratings->addChild('overall', empty($rating) ? '3' : $rating);
        $overall->addAttribute('min', '0');
        $overall->addAttribute('max', '5');

        return $review->addChild('products');
    }

    public static function getProductsFeed($variations, $products_v, $product, $sub_brand_name)
    {
        if (count($variations) <= 0) {
            return;
        }

        foreach ($variations as $key => $variation) {
            if (strlen($variation['sku']) > 7) {
                $product_v = $products_v->addChild('product');
                $product_ids = $product_v->addChild('product_ids');                
                $upc_id=$variation['upc_id'];
                if (!empty($upc_id)) {
                    $gtins = $product_ids->addChild('gtins');
                    $gtins->addChild('gtin', $upc_id);
                }
                $skus = $product_ids->addChild('skus');
                $skus->addChild('sku', $variation['sku']);
                $brands = $product_ids->addChild('brands');
                $brands->addChild('brand', $sub_brand_name);
                $name = $product->get_name() . ',' . $variation['attributes']['attribute_pa_flavour'] . ',' . $variation['weight_html'];
                $product_v->addChild('product_name', $name);
                $link = get_permalink();
                $product_v->addChild('product_url', htmlentities($link));
            }
        }
    }
}
