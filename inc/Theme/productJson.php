<?php

namespace Arcadia\Theme;


use WP_Query;

class productJson
{
  public function index()
  {
    // $data = new WP_Query(array(
    //   'post_type'      => 'product',
    //   'post_status' => 'publish',
    //   'posts_per_page' => -1,
    //   )
    // );
    $data = new WP_Query(array(
      'post_type'   => 'product',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'tax_query'   => array( array(
          'taxonomy'  => 'product_visibility',
          'terms'     => array( 'exclude-from-catalog' ),
          'field'     => 'name',
          'operator'  => 'NOT IN',
      ))
    ));

  while ($data->have_posts()) {
    $data->the_post();
    $product_post = $data->post;
    $product = wc_get_product($product_post->ID);
    $variations = $product->get_available_variations();
    if (count($variations) > 0) {      
      foreach ($variations as $key => $variation) {
        $sku = $variation['sku'];
        $upc_id=empty($variation['upc_id'])?'' :$variation['upc_id'];
        $url = get_permalink($variation['variation_id']);
        $sku_upc_url['sku_upc'][$sku]=$upc_id;
        $sku_upc_url['sku_url'][$sku]=$url;
      }
    }
  }
  
  
  echo json_encode(
    $sku_upc_url
  );
  return;
  }
}
