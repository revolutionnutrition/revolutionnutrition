<?php

namespace Arcadia\Theme;


use WP_Query;
use WC_Product_Variable;
use SimpleXMLElement;
use WC_Product_Variation;

class exportSkuUrlExcelController
{
  public function index()
  {
    $data = new WP_Query(array(
      'post_type'   => 'product',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'tax_query'   => array( array(
          'taxonomy'  => 'product_visibility',
          'terms'     => array( 'exclude-from-catalog' ),
          'field'     => 'name',
          'operator'  => 'NOT IN',
      ) )
  ) );
    $fileName = "sku_upc_url.xls";
    header("Content-Disposition: attachment; filename=\"$fileName\"");
    header("Content-Type: plain/text");
    $flag = false;
    while ($data->have_posts()) { 
      $data->the_post();
      $product_post = $data->post;
      $product = wc_get_product($product_post->ID);
      $variations = $product->get_available_variations();
      if (count($variations) > 0) {
        
        foreach ($variations as $key => $variation) {
          $sku = $variation['sku'];
          $link = get_permalink($variation['variation_id']);
          $variationObj = wc_get_product($variation['variation_id']);
          $data_v = $variationObj->get_data();
          $name_v=$data_v['name'];
          //$variationName = implode(" / ", $variation1->get_variation_attributes());
          $upc_id=$variation['upc_id'];
          //$link2 =htmlentities('<a href='.get_permalink($variation['variation_id']).'>'.get_permalink($variation['variation_id']).'</a>') ;
          $row = array(
            'title'  => $name_v,
            'id'      => $sku,
            'price'      => $variation['display_regular_price'],
            'sale price'      => $variation['display_price'],
            'gtin'=> $upc_id,
            'item group id'=>$product_post->ID,
            'URL' => $link
          );
          if (!$flag) {
            // display field/column names as first row
            echo implode("\t", array_keys($row)) . "\n";
            $flag = true;
          }
          echo implode("\t", array_values($row)) . "\n";
        }
      }
    }
    exit;
  }
}
