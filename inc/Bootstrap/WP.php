<?php

namespace Arcadia\Bootstrap;

/**
 * Place this file in a inc/Bootstrap folder within the revolution theme
 * Within inc/Setup.php add new WP(); to the bottom of the init method.
 * Add `use Arcadia\Bootstrap\WP;` to the top of the statements at the top of the file
 * After that you can make a new controller under the inc/Theme directory that will match the registerPage call
 * You will need to go to Settings > Permalinks and save after every new page registration
 *
 * For example YourController@index will open inc/Theme/YourController.php and run the index method.
 */
class WP
{
    public $namespace = 'Arcadia\Theme\\';

    /**
     * Public routes
     * @var array
     */
    protected $routes = [];

    /**
     * Kick things off
     */
    public function __construct()
    {
        if (!is_admin()) {
            // Rename to your page
            $this->registerPage('revolution-nutrition-products-feed', 'feedController@index');
            $this->registerPage('excel-feed', 'exportSkuUrlExcelController@index');
            $this->registerPage('google-feed', 'productJson@index');
            add_action('query_vars', [$this, 'additionalParams']);
            add_action('init', [$this, 'rewriteRules'], 1, 0);
            add_action('parse_request', [$this, 'processRoute']);
        }
    }

    /**
     * Used to register new pages
     */
    public function registerPage($uri, $resource)
    {
        $segments = explode('/', $uri);

        $incoming = array_map(function ($segment) {
            return substr($segment, 0, 1) === '{' ? '([^/]*)' : $segment;
        }, $segments);

        $outgoing = array_filter($segments, function ($segment) {
            return substr($segment, 0, 1) === '{';
        });

        $outgoing = array_map(function ($segment) {
            return substr($segment, 1, -1);
        }, $outgoing);

        $queryParams = [];

        foreach (array_values($outgoing) as $match => $key) {
            $queryParams[] = $key . '=$matches[' . ($match + 1) . ']';
        }

        $pageName = str_replace('/', '-', preg_replace("/[^A-Za-z0-9\/]/", '', $uri));

        list($controller, $method) = explode('@', $resource);

        $this->routes[$pageName] = [
            'incoming' => implode('/', $incoming),
            'outgoing' => 'index.php?pagename=' . $pageName . (!empty($queryParams) ? '&' . implode('&', $queryParams) : ''),
            'controller' => $controller,
            'method' => $method,
            'params' => $outgoing,
        ];
    }

    /**
     * Handles any extra query string parameters
     */
    public function additionalParams($qvars)
    {
        $params = array_reduce($this->routes, function ($carry, $item) {
            return array_merge($carry, array_values($item['params']));
        }, []);

        return array_merge($qvars, array_unique($params));
    }

    /**
     * Set up the routing for new pages
     */
    public function rewriteRules()
    {
        global $wp_rewrite;

        foreach ($this->routes as $route) {
            add_rewrite_rule($route['incoming'] . '/?', $route['outgoing'], 'top');
        }

        $wp_rewrite->flush_rules(true);
    }

    /**
     * Process the new page registrations by loading the controller and running the method
     */
    public function processRoute(\WP $wp)
    {
        if (!array_key_exists('pagename', $wp->query_vars) || !array_key_exists($wp->query_vars['pagename'], $this->routes)) {
            return;
        }

        $route = $this->routes[$wp->query_vars['pagename']];
        $controller = $this->namespace . $route['controller'];
        $resource = new $controller;
        $params = [];

        if (!empty($route['params'])) {
            $params = array_map(function ($item) use ($wp) {
                return $wp->query_vars[$item];
            }, $route['params']);
        }

        call_user_func_array([$resource, $route['method']], $params);

        exit;
    }
}