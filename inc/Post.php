<?php
/**
 * Post helper
 */
class Post
{
    /**
     * Recent posts
     * @param  integer $number Number of posts to fetch
     * @param  string  $type   Custom post type
     * @return array           Post collection
     */
    public static function recent($number = 1, $type = 'post')
    {
        $posts = new WP_Query([
            'post_type' => $type,
            'posts_per_page' => $number,
        ]);

        wp_reset_query();

        return $posts->get_posts();
    }
}
