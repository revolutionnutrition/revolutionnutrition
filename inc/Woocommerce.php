<?php

namespace Arcadia;

use Layout;
use WP_Query;
use ScheduledItem;
use WC_Points_Rewards_Product;

/**
 * Setup
 */
class Woocommerce
{
    public static $btnLabel;
    public static $hideCurrencyLabel;
    public static $isProductGroup;

    public static function init()
    {
        self::$btnLabel = get_field('product_listing_button_text', 'options');
        self::$hideCurrencyLabel = get_field('product_pricing_currency', 'options');

        add_action('woocommerce_before_cart', [__CLASS__, 'dropProductSuffix']);
        add_action('woocommerce_before_cart', [__CLASS__, 'freeGiftWarning']);
        add_action('woocommerce_price_format', [__CLASS__, 'addPriceSuffix'], 9999, 2);
        add_action('woocommerce_after_single_product_summary', 'woocommerce_template_single_excerpt', 4);
        add_action('woocommerce_after_single_product_summary', [__CLASS__, 'injectLayoutBuilder'], 5);
        add_action('add_meta_boxes', [__CLASS__, 'addVerificationMetaBox']);
        add_action('woocommerce_after_add_to_cart_button', [__CLASS__, 'continueShoppingButton'], 5);
        add_action('woocommerce_cart_collaterals', [__CLASS__, 'continueShoppingButton'], 50);
        add_action('woocommerce_product_after_variable_attributes', [__CLASS__, 'addUpcIdToVariations'], 10, 3);
        add_action('woocommerce_save_product_variation', [__CLASS__, 'saveUpcIdVariations'], 10, 2);
        add_action('woocommerce_checkout_shipping', 'woocommerce_checkout_payment', 30);
        add_action('woocommerce_checkout_shipping', [__CLASS__, 'shippingOptions'], 15);
        add_action('woocommerce_checkout_shipping', [__CLASS__, 'additionalFields'], 20);
        add_action('woocommerce_admin_order_data_after_shipping_address', [__CLASS__, 'addShippingPhoneToOrderScreen'], 10, 1);
        add_action('woocommerce_after_cart', 'woocommerce_cross_sell_display', 20, 1);
        add_action('init', [__CLASS__, 'supersizeProducts']);

        add_filter('woocommerce_product_add_to_cart_text', [__CLASS__, 'overrideProductListingButton']);
        add_filter('woocommerce_get_price_html', [__CLASS__, 'addProductPriceOverride'], 10, 2);
        add_filter('wp_update_comment_data', [__CLASS__, 'updateVerificationStatus']);
        add_filter('woocommerce_single_product_zoom_enabled', '__return_false');
        add_filter('woocommerce_sale_flash', '__return_false');
        add_filter('woocommerce_show_page_title', '__return_false');
        add_filter('woocommerce_product_tabs', [__CLASS__, 'removeProductTabs'], 9999);
        add_filter('woocommerce_variable_sale_price_html', [__CLASS__, 'priceStartingAt'], 10, 2);
        add_filter('woocommerce_variable_price_html', [__CLASS__, 'priceStartingAt'], 10, 2);
        add_filter('woocommerce_return_to_shop_redirect', [__CLASS__, 'returnToShop']);
        add_filter('woocommerce_add_to_cart_redirect', [__CLASS__, 'customAddToCartRedirect']);
        add_filter('woocommerce_add_to_cart_fragments', [__CLASS__, 'customCartFragment'], 30, 1);
        add_filter('wc_stripe_hide_payment_request_on_product_page', '__return_true');
        add_filter('woocommerce_available_variation', [__CLASS__, 'addUPCIdVariationData']);
        add_filter('woocommerce_structured_data_product', [__CLASS__, 'structuredDataProductProcessed'], 10, 2);
        add_filter('woocommerce_structured_data_type_for_page', [__CLASS__, 'structuredDataTypeProcessed'], 10, 2);
        add_filter('woocommerce_account_menu_items', [__CLASS__, 'accountNavigation'], 10, 1);
        add_filter('wc_moneris_hosted_tokenization_css_textbox', [__CLASS__, 'creditCardCSS'], 10, 1);
        add_filter('woocommerce_update_order_review_fragments', [__CLASS__, 'shippingMethodFragment'], 10, 1);
        add_filter('woocommerce_paypal_icon', [__CLASS__, 'updatedPayPalIcon']);
        add_filter('woocommerce_create_account_default_checked', '__return_true');
        add_filter('woocommerce_coupon_is_valid', [__CLASS__, 'disableCouponStacking'], 500, 2);
        add_filter('woocommerce_checkout_fields', [__CLASS__, 'addShippingPhoneNumber']);

        remove_filter('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
        remove_filter('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

        remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 10, 1);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
        remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);

        if (function_exists('WC_GC')) {
            add_action('woocommerce_checkout_shipping', [WC_GC()->cart, 'display_form'], 25);
            add_action('woocommerce_gc_after_apply_gift_card_form', [WC_GC()->cart, 'print_checkout_gift_cards']);

            self::removeFilterByMethod('woocommerce_proceed_to_checkout', 'display_form', 9);
            self::removeFilterByMethod('woocommerce_review_order_before_payment', 'display_form', 9);
        }

        if (class_exists('FGF_Gift_Products_Handler')) {
            remove_action('woocommerce_after_cart_table', [\FGF_Gift_Products_Handler::class, 'display_gift_products'], 10);
            remove_action('woocommerce_checkout_order_review', [\FGF_Gift_Products_Handler::class, 'display_gift_products']);
            remove_action('woocommerce_get_item_data', [\FGF_Gift_Products_Handler::class, 'maybe_add_custom_item_data'], 10, 2);
            add_action('woocommerce_before_cart_table', [\FGF_Gift_Products_Handler::class, 'display_gift_products']);
        }
    }

    public static function structuredDataTypeProcessed($types)
    {
        if (is_product() && self::$isProductGroup) {
            array_push($types, 'productgroup');
        }

        return $types;
    }

    public static function structuredDataProductProcessed($markup, $product)
    {
        if (!is_product() || $product->is_type( 'simple' )) {
            return $markup;
        }

        $variations = $product->get_available_variations();

        if (empty($variations)) {
            return $markup;
        }

        self::$isProductGroup = true;

        $cat_id_list = $product->get_category_ids();
        $sub_brandId_list = [293, 652, 653];
        $sub_brandId = array_values(array_intersect($cat_id_list, $sub_brandId_list));
        $sub_brand_name = empty($sub_brandId) ?  "Revolution Nutrition" : get_term($sub_brandId[0])->name;
        $currency = get_woocommerce_currency();

        $markup['hasVariant'] = [];
        $markup['@type'] = "ProductGroup";
        $markup['productGroupID'] = $product->get_id();
        $markup['brand'] = $sub_brand_name;
        $markup['gtin8'] = $product->get_id();

        foreach ($variations as $key => $variation) {
            $variationObj = wc_get_product($variation['variation_id']);
            $data=$variationObj->get_data();
            $name=$data['name'];
            array_push($markup['hasVariant'], [
                '@type' => 'Product',
                'sku' => $variation['sku'],
                'gtin8' => empty($variation['upc_id']) ? $product->get_id() : $variation['upc_id'],
                'name' => empty($name) ? "Revloution Nutrition product" : $name,
                'description' => empty($variation['variation_description']) ?
                    (empty($variation['attributes']['attribute_pa_flavour']) ? $variation['attributes']['attribute_pa_size'] : $variation['attributes']['attribute_pa_flavour']) :
                    $variation['variation_description'],
                'size' => $variation['dimensions_html'],
                'image' => $variation['image']['url'],
                'brand' => $sub_brand_name,
                'offers' => [
                    '@type' => 'Offer',
                    'priceCurrency' => $currency,
                    'availability' => 'http://schema.org/' . ($variation['is_in_stock'] ? 'InStock' : 'OutOfStock'),
                    'priceValidUntil' => date('Y-12-31', time() + YEAR_IN_SECONDS),
                    "url" => empty(get_permalink($variation['variation_id']))? get_permalink($product->get_id()) : get_permalink($variation['variation_id']),
                    'price' =>number_format($variation['display_price'], 2, '.', ''),
                ],
            ]);
        }

        return $markup;
    }

    public static function addUpcIdToVariations($loop, $variation_data, $variation)
    {
        woocommerce_wp_text_input([
            'id' => 'upc_id[' . $loop . ']',
            'class' => 'short',
            'label' => __('UPC ', 'woocommerce'),
            'value' => get_post_meta($variation->ID, 'upc_id', true),
        ]);
    }

    public static function saveUpcIdVariations($variation_id, $i)
    {
        $upc_id = $_POST['upc_id'][$i];

        if (isset($upc_id)) {
            update_post_meta($variation_id, 'upc_id', esc_attr($upc_id));
        }
    }

    public static function addUPCIdVariationData($variations)
    {
        $variations['upc_id'] = '';

        if (get_post_meta($variations['variation_id'], 'upc_id', true)) {
            $variations['upc_id'] = get_post_meta($variations['variation_id'], 'upc_id', true);
        }

        return $variations;
    }

    public static function removeFilterByMethod($hook_name = '', $method_name = '', $priority = 0)
    {
        global $wp_filter;

        if (!isset($wp_filter[ $hook_name ][ $priority ]) || !is_array($wp_filter[ $hook_name ][ $priority ])) {
            return false;
        }

        foreach ((array) $wp_filter[$hook_name][$priority] as $unique_id => $filter_array) {
            if (isset($filter_array['function']) && is_array($filter_array['function'])) {
                if (is_object($filter_array['function'][0]) && get_class($filter_array['function'][0]) && $filter_array['function'][1] == $method_name) {
                    if (is_a($wp_filter[$hook_name], 'WP_Hook')) {
                        unset($wp_filter[$hook_name]->callbacks[$priority][$unique_id]);
                    } else {
                        unset($wp_filter[$hook_name][$priority][$unique_id]);
                    }
                }
            }
        }

        return false;
    }

    public static function customCartFragment($fragments)
    {
        global $woocommerce;

        $cart = WC()->cart;

        ob_start(); ?>
        <a href="<?php echo wc_get_cart_url(); ?>" class="cart-link">
            <span class="cart-label"><?php _e('Cart', DOMAIN); ?></span> <?php echo $cart->get_cart_subtotal(); ?> <em class="far fa-shopping-basket"></em>
            <span class="total-items"><?php echo $cart->get_cart_contents_count(); ?></span>
        </a>
        <?php
        $fragments['a.cart-link'] = ob_get_clean();

        return $fragments;
    }

    public static function customAddToCartRedirect($url)
    {
        $variation_id=$_POST['variation_id'];

        return $variation_id ? $url . '?added='.$variation_id : $url . '?added';
    }

    public static function priceStartingAt($price, $product)
    {
        $reg_min_price = $product->get_variation_regular_price('min', true);
        $min_sale_price = $product->get_variation_sale_price('min', true);
        $max_price = $product->get_variation_price('max', true);
        $min_price = $product->get_variation_price('min', true);

        $price = ($min_sale_price == $reg_min_price) ?
            wc_price($reg_min_price) :
            '<del>' . wc_price($reg_min_price) . '</del> <ins>' . wc_price($min_sale_price) . '</ins>';

        return $price;
    }

    public static function overrideProductListingButton($default)
    {
        if (self::$btnLabel) {
            return self::$btnLabel;
        }

        return $default;
    }

    public static function addPriceSuffix($format, $currency_pos)
    {
        if (self::$hideCurrencyLabel) {
            return $format;
        }

        switch ($currency_pos) {
            case 'left':
                $currency = get_woocommerce_currency();
                $format = '%1$s%2$s&nbsp;' . $currency;
                break;
        }

        return $format;
    }

    public static function addProductPriceOverride($price, $class)
    {
        $skipped = false;

        if (get_field('product_suffix', 'options')) {
            foreach (get_field('product_suffix', 'options') as $data) {
                $scheduled = new ScheduledItem($data);
                $id = $class->get_parent_id();

                if ($id === 0) {
                    $id = $class->get_id();
                }

                $color = $data['suffix_colour'];

                if (!$color) {
                    $color = 'red';
                }

                if ($scheduled->isVisible() && (empty($data['products']) || in_array($id, $data['products']))) :
                    if (!$skipped) {
                        if ($color === 'custom') :
                            $price .= ' <span class="price-suffix bg-' . $color . '"><span class="price-badge"';
                            $price .= ' style="background-color: ' . $data['background_color'] . '; color: ' . $data['text_color'] . '"';
                            $price .= '>';
                        else :
                            $price .= ' <span class="price-suffix bg-' . $color . '"><span class="price-badge">';
                        endif;
                    }

                    $price .= $data['suffix'];
                    $skipped = true;
                endif;
            }

            if ($skipped) {
                $price .= '</span></span>';
            }
        }

        if (get_field('price_suffix') && !$skipped) {
            $color = get_field('suffix_colour');

            if (!$color) {
                $color = 'red';
            }

            $price .= ' <span class="price-suffix bg-' . $color . '"><span class="price-badge">' . get_field('price_suffix') . '</span></span>';
        }

        if ($class->is_on_sale()) {
            $savings = wc_get_price_to_display($class, ['price' => $class->get_regular_price()]) - wc_get_price_to_display($class);

            if ($savings > 0) {
                $price .= ' <span class="savings">Save <small>$</small>' . number_format($savings, 2) . '</span>';
            }
        }

        $points_earn = WC_Points_Rewards_Product::get_points_earned_for_product_purchase($class);

        if ($points_earn > 0) {
            $price .= ' <span class="fitcoin-earn">Earn $<span class="number">' . number_format_i18n($points_earn, 2) . '</span></span>';
        }

        return $price;
    }

    public static function dropProductSuffix()
    {
        remove_filter('woocommerce_get_price_html', [__CLASS__, 'addProductPriceOverride']);
    }

    public static function injectLayoutBuilder()
    {
        if (get_field('blocks')) {
            echo '<div class="product-learn-more"><a href="#block1">' . __('Learn more', DOMAIN) . '</a></div>';
        }

        Layout::render([
            'default' => null,
        ]);
    }

    public static function verificationStatus($comment)
    {
        $verified = intval(get_comment_meta($comment->comment_ID, 'verified', true)); ?>
            <table class="form-table editcomment comment_xtra">
            <tbody>
                <tr valign="top">
                    <td><input type="checkbox" name="verified" value="1"<?php echo $verified === 1 ? 'checked' : ''; ?> /> Verified</td>
                </tr>
           </tbody>
           </table>
        <?php
    }

    public static function addVerificationMetaBox()
    {
        add_meta_box('verificationStatus', 'Verification Status', [__CLASS__, 'verificationStatus'], 'comment', 'normal');
    }

    public static function updateVerificationStatus($comment)
    {
        update_comment_meta($comment['comment_ID'], 'verified', isset($_POST['verified']) ? 1 : 0);
        return $comment;
    }

    public static function continueShoppingButton()
    {
        echo '<a href="' . site_url('/product-category/all-products/') . '" class="button">' . __('continue shopping', DOMAIN) . '</a>';
    }

    public static function returnToShop()
    {
        return site_url('/product-category/all-products/');
    }

    public static function removeProductTabs($tabs)
    {
        unset($tabs['additional_information']);
        return $tabs;
    }

    public static function freeGiftWarning()
    {
        global $woocommerce;

        $subtotal = WC()->cart->get_subtotal();

        if (get_field('promotions', 'options')) {
            foreach (get_field('promotions', 'options') as $promotion) {
                $scheduled = new ScheduledItem($promotion);

                if ($subtotal < $promotion['minimum_amount']) {
                    // just for free shipping messsage
                    if (preg_match('[free_shipping_min_sub_total]', $promotion['text'])) {
                        $promotion['text'] = preg_replace('/\[free_shipping_min_sub_total\]/', wc_price($promotion['minimum_amount'] - $subtotal), $promotion['text']);
                        echo '<div class="woocommerce-message" role="alert">'. $promotion['text'] . '</div>';
                    }
                    continue;
                } elseif (preg_match('/\[free_shipping_min_sub_total\]/', $promotion['text'])) {
                    continue;
                }

                if (in_array(strtolower($promotion['coupon_code']), $woocommerce->cart->get_applied_coupons())) {
                    continue;
                }

                if (!$scheduled->isVisible()) {
                    continue;
                }

                $code = $promotion['coupon_code'] ? ' <a class="code" href="#" data-code="'. $promotion['coupon_code'] .'">Apply Code</a>' : '';

                echo '<div class="woocommerce-message" role="alert">'. $promotion['text'] . $code .'</div>';
            }
        }
    }

    public static function accountNavigation($items)
    {
        unset($items['customer-logout']);

        $items['points-and-rewards'] = __('Fitcoin Wallet', DOMAIN);

        return $items;
    }

    public static function shippingOptions()
    {
        $packages = WC()->shipping()->get_packages();

        if (count($packages) > 0) {
            wc_cart_totals_shipping_html();
        } else {
            wc_get_template('checkout/checkout-shipping-empty.php', []);
        }
    }

    public static function additionalFields()
    {
        wc_get_template('checkout/additional-fields.php', [
            'checkout' => \WC_Checkout::instance(),
        ]);
    }

    public static function creditCardCSS()
    {
        return 'width:calc(100% - 1px);border-radius:0;font-size:16px;color:rgb(0,0,0);padding:8px 12px;border:1px solid rgb(203,201,201);line-height:1.2em;background-color:rgb(255,255,255);margin:0;';
    }

    public static function shippingMethodFragment($data)
    {
        $packages = WC()->shipping()->get_packages();

        ob_start();

        if (count($packages) > 0) {
            wc_cart_totals_shipping_html();
        } else {
            wc_get_template('checkout/checkout-shipping-empty.php', []);
        }

        $data['.woocommerce-shipping-totals'] = ob_get_clean();

        return $data;
    }

    public static function cartShippingOptions()
    {
        $packages = WC()->shipping()->get_packages();
        $first    = true;

        foreach ($packages as $i => $package) {
            $chosen_method = isset(WC()->session->chosen_shipping_methods[ $i ]) ? WC()->session->chosen_shipping_methods[ $i ] : '';
            $product_names = array();

            if (count($packages) > 1) {
                foreach ($package['contents'] as $item_id => $values) {
                    $product_names[ $item_id ] = $values['data']->get_name() . ' &times;' . $values['quantity'];
                }
                $product_names = apply_filters('woocommerce_shipping_package_details_array', $product_names, $package);
            }

            wc_get_template(
                'cart/old-shipping.php',
                array(
                    'package'                  => $package,
                    'available_methods'        => $package['rates'],
                    'show_package_details'     => count($packages) > 1,
                    'show_shipping_calculator' => is_cart() && apply_filters('woocommerce_shipping_show_shipping_calculator', $first, $i, $package),
                    'package_details'          => implode(', ', $product_names),
                    /* translators: %d: shipping package number */
                    'package_name'             => apply_filters('woocommerce_shipping_package_name', (($i + 1) > 1) ? sprintf(_x('Shipping %d', 'shipping packages', 'woocommerce'), ($i + 1)) : _x('Shipping', 'shipping packages', 'woocommerce'), $i, $package),
                    'index'                    => $i,
                    'chosen_method'            => $chosen_method,
                    'formatted_destination'    => WC()->countries->get_formatted_address($package['destination'], ', '),
                    'has_calculated_shipping'  => WC()->customer->has_calculated_shipping(),
                )
            );

            $first = false;
        }
    }

    public static function updatedPayPalIcon()
    {
        return get_site_url() . '/wp-content/uploads/2021/02/paypal-logo.svg';
    }

    public static function disableCouponStacking($enabled, $coupon)
    {
        $existingCoupons = WC()->cart->get_applied_coupons();
        $unstackableCriteria = get_field('unstackable_coupons', 'options');
        $currentCoupon = $coupon->get_code();

        if (!$unstackableCriteria || in_array($currentCoupon, $existingCoupons)) {
            return $enabled;
        }

        $unstackableCriteria = array_map(function ($row) {
            return $row['prefix'];
        }, $unstackableCriteria);

        $matchingPrefixes = array_filter($unstackableCriteria, function ($prefix) use ($currentCoupon) {
            return !empty($prefix) && substr($currentCoupon, 0, strlen($prefix)) === $prefix;
        });

        if (empty($matchingPrefixes)) {
            return $enabled;
        }

        $matchingExisting = array_filter($matchingPrefixes, function ($prefix) use ($existingCoupons) {
            $matches = array_filter($existingCoupons, function ($coupon) use ($prefix) {
                return substr($coupon, 0, strlen($prefix)) === $prefix;
            });

            return !empty($matches);
        });

        if (!empty($matchingExisting)) {
            add_filter('woocommerce_coupon_error', function () {
                return __('Coupon can\'t be combined', DOMAIN);
            }, 999);

            return false;
        }

        return $enabled;
    }

    public static function addShippingPhoneNumber($fields)
    {
        $fields['shipping']['shipping_phone'] = [
            'label' => __('Phone', 'woocommerce'),
            'placeholder' => _x('Phone', 'placeholder', 'woocommerce'),
            'required' => false,
            'class' => ['form-row-wide'],
            'clear' => true,
        ];

        return $fields;
    }

    public static function addShippingPhoneToOrderScreen($order)
    {
        $phone = get_post_meta($order->get_id(), '_shipping_phone', true);

        if ($phone) {
            echo '<p>';
            echo '<strong>' . __('Phone', 'woocommerce') . ':</strong> ';
            echo get_post_meta($order->get_id(), '_shipping_phone', true);
            echo '</p>';
        }
    }

    public static function supersizeProducts()
    {
        if (isset($_GET['supersize']) && isset($_GET['from']) && isset($_GET['to']) && isset($_GET['qty'])) {
            $fromVariadtionId = $_GET['from'];
            $toVariationId = $_GET['to'];
            $qty = $_GET['qty'];

            $mergeQty = 0;
            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                $variation   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                $variationId = $variation->get_variation_id();

                if ($toVariationId == $variationId) {
                    $mergeQty = $qty + $cart_item['quantity'];
                    WC()->cart->remove_cart_item($cart_item_key);
                }

                if ($fromVariadtionId == $variationId) {
                    WC()->cart->remove_cart_item($cart_item_key);
                }
            }

            if ($mergeQty > 0) {
                WC()->cart->add_to_cart($toVariationId, $mergeQty);
            } else {
                WC()->cart->add_to_cart($toVariationId, $qty);
            }

            wp_redirect(home_url() . '/cart');
            exit();
        }
    }
}
