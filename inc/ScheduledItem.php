<?php
/**
 * ScheduledItem
 */
class ScheduledItem
{
    /**
     * data
     * @var array
     */
    private $data;

    /**
     * Visibility timezone
     * @var object
     */
    private $timezone;

    /**
     * Create new block
     * @param  array $data Block fields
     * @return void
     */
    public function __construct($data = [])
    {
        $this->timezone = new DateTimeZone(get_option('timezone_string'));
        $this->data = $data;
    }

    /**
     * Fetch all data
     * @return array
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * Check existance of data within block
     * @param  string  $field Field name
     * @return boolean
     */
    private function fieldExists($field)
    {
        return array_key_exists($field, $this->data) && !empty($this->data[$field]);
    }

    /**
     * Return DateTime object for starting visibility
     * @return object Start Date
     */
    public function visibleFrom()
    {
        return new DateTime($this->data['publish_on'], $this->timezone);
    }

    /**
     * Return DateTime object for ending visibility
     * @return object End Date
     */
    public function visibleUntil()
    {
        return new DateTime($this->data['expire_on'], $this->timezone);
    }

    /**
     * Check visibility
     * @return boolean
     */
    public function isVisible()
    {
        $now = new DateTime('now', $this->timezone);

        if (($this->fieldExists('expire_on') && $this->visibleUntil() < $now) ||
            ($this->fieldExists('publish_on') && $this->visibleFrom() > $now)) {
            return false;
        }

        return true;
    }
}
