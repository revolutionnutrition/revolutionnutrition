<?php

namespace Arcadia\Helper;

/**
 * Loop Helper
 */
class Loop
{
    /**
     * Whether current iteration is the first
     * @var boolean
     */
    public $first = true;

    /**
     * Whether current iteration is the last
     * @var boolean
     */
    public $last = true;

    /**
     * Current iteration
     * @var integer
     */
    public $index = 0;

    /**
     * Total iterations
     * @var integer
     */
    public $count = 1;

    /**
     * Set up loop counter
     * @param  integer $count Total number of iterations
     * @return void
     */
    public function __construct($count)
    {
        $this->count = $count;
        $this->last = $count === 1;
    }

    /**
     * Set specific index
     * @param  interger $index Current iteration
     * @return void
     */
    private function setIndex($index)
    {
        $this->index = $index;
        $this->first = $index === 0;
        $this->last = $index === $this->count - 1;
    }

    /**
     * Bump index by 1
     * @return void
     */
    public function iterate()
    {
        $this->setIndex($this->index + 1);
    }
}
