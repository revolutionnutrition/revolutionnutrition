<?php get_header(); ?>

    <main class="main-content" role="main" id="main" tabindex="-1">
        <?php if (is_singular('product')) : ?>
            <div class="product-body">
                <div class="container">
                    <?php woocommerce_content(); ?>
                </div>
            </div>
        <?php else : ?>
            <?php woocommerce_get_template('archive-product.php'); ?>
        <?php endif; ?>
    </main>

<?php get_footer(); ?>
