<div class="<?php Layout::classes('banner banner-home'); ?>" style="<?php Layout::partial('background'); ?>">
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php if (Field::exists('banner_title')) : ?>
            <h1><?php Field::display('banner_title'); ?></h1>
        <?php endif; ?>
        <?php Field::display('banner_desc') ?>
    </div>
</div>
