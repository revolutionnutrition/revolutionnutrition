<div class="<?php Layout::classes('banner banner-default'); ?>" style="<?php Layout::partial('background'); ?>">
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php if (Field::anyExist('banner_title1', 'banner_title2')) : ?>
            <h1>
                <?php Field::display('banner_title1'); ?>
                <?php Field::html('banner_title2', '<strong>%s</strong>'); ?>
            </h1>
        <?php endif; ?>
        <?php Field::display('banner_desc') ?>
    </div>
</div>
