<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php

    if (is_search()) {
        echo '<meta name="robots" content="noindex, nofollow">' . PHP_EOL;
    }

    ?>
    <meta name="title" content="<?php wp_title('|', true, 'right'); ?>">
    <meta name="Copyright" content="Copyright <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php

    if (get_field('mobile_theme_colour', 'option')) {
        echo '<meta name="theme-color" content="' . get_field('mobile_theme_colour', 'option') . '">' . PHP_EOL;
    }

    if (get_field('ios_status_bar', 'option')) {
        echo '<meta name="apple-mobile-web-app-status-bar-style" content="' . get_field('ios_status_bar', 'option') . '">' . PHP_EOL;
    }

    ?>
    <meta name="p:domain_verify" content="b12c40575342179ef6be18c41960779b"/>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&family=Michroma&display=swap" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <a href="#main" class="skip-link"><?php _e('Skip to content', DOMAIN); ?></a>

    <header class="header" role="banner">
        <?php Layout::partial('country-header-popup'); ?>
        <?php Layout::partial('announcements'); ?>

        <nav class="navigation" role="navigation" aria-label="site">
            <div class="container">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home" class="logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                </a>

                <div class="primary-navigation">
                    <div class="auxiliary-content">
                        <div><?php Layout::partial('country-selector-primary'); ?></div>
                        <span class="social"><?php Layout::partial('social'); ?></span>
                        <?php if (is_user_logged_in()) : ?>
                            <?php $user = wp_get_current_user(); ?>
                            <span><?php _e('Welcome', DOMAIN); ?>, <a href="<?php the_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><?php echo $user->display_name; ?></a> <a href="<?php echo wp_logout_url(home_url('/')); ?>"><?php _e('Logout', DOMAIN); ?></a></span>
                        <?php else : ?>
                            <span><?php _e('Welcome', DOMAIN); ?>, <a href="<?php the_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><?php _e('Login', DOMAIN); ?></a></span>
                        <?php endif; ?>
                    </div>
                    <div class="main-nav">
                        <?php wp_nav_menu([
                            'theme_location' => 'primary',
                        ]); ?>
                        <?php $cart = WC()->cart; ?>
                        <div class="cart-box">
                            <a href="<?php echo wc_get_cart_url(); ?>" class="cart-link">
                                <span class="cart-label"><?php _e('Cart', DOMAIN); ?></span> <?php echo $cart->get_cart_subtotal(); ?> <em class="far fa-shopping-basket"></em>
                                <span class="total-items"><?php echo $cart->get_cart_contents_count(); ?></span>
                            </a>

                            <div class="mini-cart-popout">
                                <div class="widget_shopping_cart_content">
                                    <?php woocommerce_mini_cart(); ?>
                                </div>
                            </div>
                        </div>
                        <?php get_search_form(); ?>
                    </div>
                    <div class="mobile-nav">
                        <?php wp_nav_menu([
                            'theme_location' => 'mobile',
                            'depth' => 1,
                        ]); ?>
                        <button type="button" class="open-mobile-menu">
                            <em class="far fa-bars"></em>
                        </button>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <div class="mobile-close-target"></div>

    <div class="mobile-navigation">
        <div class="welcome">
            <?php if (is_user_logged_in()) : ?>
                <?php $user = wp_get_current_user(); ?>
                <a href="<?php the_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><?php echo $user->display_name; ?></a> <a href="<?php echo wp_logout_url(home_url('/')); ?>"><?php _e('Logout', DOMAIN); ?></a>
            <?php else : ?>
                <?php _e('Welcome', DOMAIN); ?>, <a href="<?php the_permalink(get_option('woocommerce_myaccount_page_id')); ?>"><?php _e('Login', DOMAIN); ?></a>
            <?php endif; ?>
        </div>

        <?php wp_nav_menu([
            'theme_location' => 'mobileham',
            'depth' => 1,
        ]); ?>

        <div class="country-selector-mobile-outer">
            <?php Layout::partial('country-selector-mobile'); ?>
        </div>
        <div class="social"><?php Layout::partial('social'); ?></div>
    </div>
