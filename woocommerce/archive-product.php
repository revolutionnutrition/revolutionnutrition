<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$term = get_queried_object();

$image_desk = get_field('desktop_image', $term);
$image_mob = get_field('mobile_image', $term);

?>
<?php if ($term && !empty($term->description)) : ?>
    <header class="woocommerce-category-header">
        <div class="container">
            <h1 class="page-title">
                <?php woocommerce_page_title(); ?>
            </h1>

            <?php if (strpos($term->description, '<!--more-->')) : ?>
                <?php $parts = explode('<!--more-->', $term->description); ?>
                <?php echo wc_format_content($parts[0]); ?>
                <div class="long-description">
                    <div class="inner">
                        <?php echo wc_format_content($parts[1]); ?>
                    </div>
                </div>
                <button type="button" class="desc-read-more">Read More</button>
            <?php else : ?>
                <?php echo wc_format_content($term->description); ?>
            <?php endif; ?>
        </div>
    </header>
<?php endif; ?>

<?php if ($image_desk) :
    $desktop = $image_desk['url'];
    $alt_desk = $image_desk['alt'];
    ?>
    <div class="product_banner_desk">
        <img src="<?php echo $desktop;?>" alt="<?php echo $alt_desk ?>">
    </div>
<?php endif; ?>

<?php if ($image_mob) :
    $mobile = $image_mob['url'];
    $alt_mob = $image_mob['alt'];
    ?>
    <div class="product_banner_mob">
        <img src="<?php echo $mobile;?>" alt="<?php echo $alt_mob ?>">
    </div>
<?php endif; ?>

<div class="product-body">
    <div class="container">
        <div class="fitcoin-switch-bar">
            <div class="toggle-switch-wrapper">
            <span class="fit__text">Show Fitcoin Rewards</span>
                <label class="toggle-switch">
                    <input id="fitcoin-toggle-switch" type="checkbox" autocomplete="off">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
        <?php
        if ( woocommerce_product_loop() ) {

            /**
             * Hook: woocommerce_before_shop_loop.
             *
             * @hooked woocommerce_output_all_notices - 10
             * @hooked woocommerce_result_count - 20
             * @hooked woocommerce_catalog_ordering - 30
             */
            do_action( 'woocommerce_before_shop_loop' );

            woocommerce_product_loop_start();

            if ( wc_get_loop_prop( 'total' ) ) {
                while ( have_posts() ) {
                    the_post();

                    /**
                     * Hook: woocommerce_shop_loop.
                     */
                    do_action( 'woocommerce_shop_loop' );

                    wc_get_template_part( 'content', 'product' );
                }
            }

            woocommerce_product_loop_end();

            /**
             * Hook: woocommerce_after_shop_loop.
             *
             * @hooked woocommerce_pagination - 10
             */
            do_action( 'woocommerce_after_shop_loop' );
        } else {
            /**
             * Hook: woocommerce_no_products_found.
             *
             * @hooked wc_no_products_found - 10
             */
            do_action( 'woocommerce_no_products_found' );
        }
        ?>
    </div>
</div>
