<?php
/**
 * WooCommerce Points and Rewards
 *
 * @package     WC-Points-Rewards/Templates
 * @author      WooThemes
 * @copyright   Copyright (c) 2013, WooThemes
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if (! defined('ABSPATH')) {
    exit;
}

/**
 * My Account - My Points
 */

$user = wp_get_current_user(); ?>
<div class="gift-card-balance">
    <div class="fitcoin-wallet-holder">
        <img src="<?php echo get_template_directory_uri(); ?>/img/fitcoincard-noname.png" alt="Wallet">
        <div class="point-user-name">
            <?php echo $user->display_name; ?>
        </div>
        <div class="point-container">
            <div class="points-amount"><?php printf(__("%.2f", 'woocommerce-points-and-rewards'), $points_balance); ?></div>
            <div class="points-label"><?php printf(__("%s", 'woocommerce-points-and-rewards'), $points_label); ?></div>
        </div>
    </div>
    <div>
        <h2 style='text-transform: none;margin-bottom:0.8rem;'>Welcome to the New Revolution Fitcoin Digital Money System</h2>
        <span>Your rewards points have been upgraded to our new Fitcoin rewards Program at an equal value</span>
    </div>
</div>

<?php if ($events) : ?>
    <table class="shop_table shop_table_responsive my_account_points_rewards my_account_orders">
        <thead>
            <tr>
                <th class="points-rewards-event-description"><?php _e('Event', 'woocommerce-points-and-rewards'); ?></th>
                <th class="points-rewards-event-date"><?php _e('Date', 'woocommerce-points-and-rewards'); ?></th>
                <th class="points-rewards-event-points"><?php echo esc_html($points_label); ?></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($events as $event) : ?>
            <tr class="points-event">
                <td class="points-rewards-event-description" data-title="<?php _e('Event', 'woocommerce-points-and-rewards'); ?>">
                    <?php echo $event->description; ?>
                </td>
                <td class="points-rewards-event-date" data-title="<?php _e('Date', 'woocommerce-points-and-rewards'); ?>">
                    <?php echo esc_html($event->date_display_human); ?>
                </td>
                <td class="points-rewards-event-points" data-title="<?php echo esc_html($points_label); ?>">
                    <?php echo ($event->points > 0 ? '+' : '') . $event->points; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
    <?php if ($current_page != 1) : ?>
        <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url(wc_get_endpoint_url('points-and-rewards', $current_page - 1)); ?>"><?php _e('Previous', 'woocommerce-points-and-rewards'); ?></a>
    <?php endif; ?>

    <?php if ($current_page * $count < $total_rows) : ?>
        <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url(wc_get_endpoint_url('points-and-rewards', $current_page + 1)); ?>"><?php _e('Next', 'woocommerce-points-and-rewards'); ?></a>
    <?php endif; ?>
    </div>

<?php endif;
