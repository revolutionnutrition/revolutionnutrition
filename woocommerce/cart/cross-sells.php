<?php
/**
 * Cross-sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cross-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */
defined('ABSPATH') || exit;
$cart_products = WC()->cart->get_cart();
$count_cart_bulk = 0;
$bulk_id = get_category_id('Bulk');

cal_count_cart_bulk($cart_products, $count_cart_bulk, $bulk_id);

if ($cross_sells) {
    $count_sell_bulk = 0;
    $count_sell_rn = 0;
    cal_sell_rn_bulk($cross_sells, $count_sell_bulk, $count_sell_rn, $bulk_id);
    /*Always showing only 4 products random products, keep 1 revolution product and only one bulk */
    $sell_length = count($cross_sells);
    if ($sell_length < 4) {
        /*if bulk item in cart is 0, but still cross sell-item have bulks, keep 1 bulk product and 3 RN */
        if ($count_cart_bulk == 0) {
            if ($count_sell_bulk > 1) {
                removeBulk($cross_sells, $count_sell_bulk - 1, $bulk_id);
                setRnSells($cross_sells, 3 - $count_sell_rn);
            } else {
                setRnSells($cross_sells, 4 - $count_sell_rn);
            }
        }
        /*if cart items all bulk , always keep 2 bulk products and 2 RN */
        if ($count_cart_bulk > 0 && ($count_cart_bulk == count($cart_products))) {
            if ($count_sell_bulk <= 2) {
                setBulkSells($cross_sells, 2 - $count_sell_bulk);
                setRnSells($cross_sells, 2);
            } elseif (($count_sell_bulk) > 2) { //keep 2 bulk, 2 RN
                if ($count_sell_rn > 0) {
                    setRnSells($cross_sells, 1);
                } else {
                    removeBulk($cross_sells, 1, $bulk_id);
                    setRnSells($cross_sells, 2);
                }
            }
        }
        /*if cart items include more than 1 RN , always keep 1 bulk product and 3 RN */
        if ($count_cart_bulk > 0 && $count_cart_bulk < count($cart_products)) {
            switch (true) {
                case ($count_sell_rn == 0 && $count_sell_bulk == 0):
                    setRnSells($cross_sells, 3);
                    setBulkSells($cross_sells, 1);

                    break;
                case ($count_sell_rn > 0 && $count_sell_bulk > 0):
                    removeBulk($cross_sells, $count_sell_bulk - 1, $bulk_id);
                    setRnSells($cross_sells, 3 - $count_sell_rn);

                    break;
                case ($count_sell_rn > 0 && $count_sell_bulk == 0):
                    setBulkSells($cross_sells, 1);
                    setRnSells($cross_sells, 3 - $count_sell_rn);

                    break;
                case ($count_sell_rn == 0 && $count_sell_bulk > 0):
                    removeBulk($cross_sells, $count_sell_bulk - 1, $bulk_id);
                    setRnSells($cross_sells, 3);

                    break;
            }
        }
    } else { //>4 same display logic as <4
        if ($count_cart_bulk == 0) {
            array_splice($cross_sells, 4);
        } elseif ($count_cart_bulk > 0 && ($count_cart_bulk == count($cart_products))) {/*if cart items all bulk , always keep 2 bulk product and 2 RN */
            if ($count_sell_bulk <= 2) {
                setBulkSells($cross_sells, 2 - $count_sell_bulk);
                removeRnSells($cross_sells, $count_sell_rn - 2, $bulk_id);
            } elseif (($count_sell_bulk) > 2) { //keep 2 bulk, 2 RN
                removeBulk($cross_sells, $count_sell_bulk - 2, $bulk_id);
                if ($count_sell_rn >= 2) {
                    removeRnSells($cross_sells, $count_sell_rn - 2, $bulk_id);
                } else {
                    setRnSells($cross_sells, 2 - $count_sell_rn);
                }
            }
        } elseif ($count_cart_bulk > 0 && $count_cart_bulk < count($cart_products)) {
            if ($count_sell_bulk > 1) {
                removeBulk($cross_sells, $count_sell_bulk - 1, $bulk_id);
            } else {
                setBulkSells($cross_sells, 1 - $count_sell_bulk);
            }
            if ($count_sell_rn > 3) {
                removeRnSells($cross_sells, $count_sell_rn - 3, $bulk_id);
            } else {
                setRnSells($cross_sells, 3 - $count_sell_rn);
            }
        }
    }
} else { //same display logic as <4
    $cross_sells = [];
    if ($count_cart_bulk > 0 && ($count_cart_bulk == count($cart_products))) {
        setRnSells($cross_sells, 2);
        setBulkSells($cross_sells, 2);
    } elseif ($count_cart_bulk > 0 && ($count_cart_bulk < count($cart_products))) {
        setRnSells($cross_sells, 3);
        setBulkSells($cross_sells, 1);
    } elseif ($count_cart_bulk == 0) {
        setRnSells($cross_sells, 4);
    }
}
$lastest_sell_length = count($cross_sells);
add_filter('woocommerce_get_price_html', [\Arcadia\Woocommerce::class, 'addProductPriceOverride'], 10, 2);
?>
<?php if ($lastest_sell_length > 0) : ?>
    <div class="cross-sells">
        <?php
        $heading = apply_filters('woocommerce_product_cross_sells_products_heading', __('Customers also bought&hellip;', 'woocommerce'));
        if ($heading) : ?>
            <h4 class="heading-cross-sells"><?php echo esc_html($heading); ?></h4>
        <?php endif; ?>

        <div class="for_cross-sells-ul">

        <?php woocommerce_product_loop_start(); ?>


        <?php foreach ($cross_sells as $cross_sell) : ?>
            <?php
            $id = $cross_sell->get_id();
            $post_object = get_post($cross_sell->get_id());
            setup_postdata($GLOBALS['post'] = &$post_object);
            wc_get_template_part('content', 'product');
            ?>
        <?php endforeach; ?>




        <?php woocommerce_product_loop_end(); ?>
        </div>

    </div>
<?php endif;

wp_reset_postdata();
