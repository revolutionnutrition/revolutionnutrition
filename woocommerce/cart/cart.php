<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_cart'); ?>

<div class="cart-container">
    <form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
        <?php do_action('woocommerce_before_cart_table'); ?>

        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
            <thead>
                <tr>
                    <th class="product-name" colspan="3"><?php esc_html_e('Product', 'woocommerce'); ?></th>
                    <th class="product-price"><?php esc_html_e('Price', 'woocommerce'); ?></th>
                    <th class="product-quantity"><?php esc_html_e('Quantity', 'woocommerce'); ?></th>
                    <th class="product-subtotal"><?php esc_html_e('Total', 'woocommerce'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php do_action('woocommerce_before_cart_contents'); ?>

                <?php
                foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                    $_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                    $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                        $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key); ?>
                        <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                            <td class="product-remove">
                                <?php echo apply_filters(
                                    'woocommerce_cart_item_remove_link',
                                    sprintf(
                                        '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><em class="far fa-times"></em></a>',
                                        esc_url(wc_get_cart_remove_url($cart_item_key)),
                                        esc_html__('Remove this item', 'woocommerce'),
                                        esc_attr($product_id),
                                        esc_attr($_product->get_sku())
                                    ),
                                    $cart_item_key
                                ); ?>
                            </td>

                            <td class="product-thumbnail">
                            <?php
                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                            if (! $product_permalink) {
                                echo $thumbnail;
                            } else {
                                printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail);
                            } ?>
                            </td>

                            <td class="product-name product-custom-img" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                            <?php
                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                            if (!$product_permalink) {
                                printf('<span class="mobileCartImage">%s</span>', $thumbnail);
                            } else {
                                printf('<a href="%s" class="mobileCartImage">%s</a>', esc_url($product_permalink), $thumbnail);
                            }

                            if (!$product_permalink) {
                                echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<span><span class="base-name">%s</span></span>', str_replace(' - ', '</span><span class="attribute-name">', $_product->get_name())), $cart_item, $cart_item_key));
                            } else {
                                echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s"><span class="base-name">%s</span></a>', esc_url($product_permalink), str_replace(' - ', '</span><span class="attribute-name">', $_product->get_name())), $cart_item, $cart_item_key));
                            }

                            do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                            echo '<div class="for-gift-card-desktop">' . wc_get_formatted_cart_item_data($cart_item) . '</div>';

                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
                            } ?>
                            </td>

                            <?php
                            if(wc_get_formatted_cart_item_data($cart_item) != null){
                                ?>
                                <td class="for-gift-card"> 
                                    <?php echo '<div>' . wc_get_formatted_cart_item_data($cart_item) . '</div>';  ?> 
                                </td>
                                <?php
                            }
                            else{
                                 echo wc_get_formatted_cart_item_data($cart_item);
                            }
                            ?>

                            <td class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                                <?php
                                    if ($_product->get_regular_price() > $_product->get_price()) {
                                        $itemPriceElem = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                                        if (strpos($itemPriceElem, '<del>') === false) {
                                            echo '<del>' .
                                            wc_price(wc_get_price_to_display( $_product, array( 'price' => $_product->get_regular_price() ) )) .
                                            '</del><br>';
                                        }
                                    }
                                ?>
                                <?php
                                    echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); ?>
                            </td>

                            <td class="product-quantity" data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
                            <?php
                            if ($_product->is_sold_individually()) {
                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                            } else {
                                $product_quantity = woocommerce_quantity_input(
                                    array(
                                        'input_name'   => "cart[{$cart_item_key}][qty]",
                                        'input_value'  => $cart_item['quantity'],
                                        'max_value'    => $_product->get_max_purchase_quantity(),
                                        'min_value'    => '0',
                                        'product_name' => $_product->get_name(),
                                    ),
                                    $_product,
                                    false
                                );
                            }

                        echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item); // PHPCS: XSS ok.
                            ?>
                            </td>

                            <td class="product-subtotal" data-title="<?php esc_attr_e('Subtotal', 'woocommerce'); ?>">
                                <?php
                                    echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                                ?>
                            </td>
                        </tr>
                        <?php
                        // start supersize function

                        // get supersize available list
                        $supersizeList = get_option('rn_supersize_list');
                        if ($supersizeList) {
                            $supersizeList = str_replace("'", '"', $supersizeList);
                            $supersizeList = json_decode($supersizeList);
                            foreach ($supersizeList as $productSizes) {
                                if ($productSizes->id == $product_id) {
                                    $supersizeRange = explode(',', $productSizes->size_list);

                                    $attrs = $_product->get_variation_attributes();
                                    $currentFlavour = $attrs['attribute_pa_flavour'];
                                    $currentSizeStr = wc_get_formatted_variation( [ 'attribute_pa_size' => $attrs['attribute_pa_size'] ], true, false);
                                    $currentSize = floatval(preg_replace('/[^0-9\.]/', '', $currentSizeStr));
                                    $currentSizeUnit = preg_replace('/[0-9\.]+/', '', $currentSizeStr);
                                    $parent = $_product->parent;
                                    $variations = $parent->get_available_variations();
                                    // sort by size
                                    usort($variations, function ($a, $b) {
                                        $aSizeStr = $a['attributes']['attribute_pa_size'];
                                        $aSizeStr = wc_get_formatted_variation( ['attribute_pa_size' => $aSizeStr ], true, false);
                                        $aSizeUnit = preg_replace('/[0-9\.]+/', '', $aSizeStr);
                                        $aSize = floatval(preg_replace('/[^0-9\.]/', '', $aSizeStr));
                                        $aSize = strpos(strtoupper($aSizeUnit), 'KG') !== false ? $aSize * 1000 : $aSize;


                                        $bSizeStr = $b['attributes']['attribute_pa_size'];
                                        $bSizeStr = wc_get_formatted_variation( ['attribute_pa_size' => $bSizeStr ], true, false);
                                        $bSizeUnit = preg_replace('/[0-9\.]+/', '', $bSizeStr);
                                        $bSize = floatval(preg_replace('/[^0-9\.]/', '', $bSizeStr));
                                        $bSize = strpos(strtoupper($bSizeUnit), 'KG') !== false ? $bSize * 1000 : $bSize;

                                        return $aSize == $bSize ? 0 : ($aSize < $bSize ? -1 : 1);
                                    });

                                    $suggestion = '';
                                    // check variation with next size and same flavour
                                    $nextSizeStr = '';
                                    $nextSize = 0;
                                    foreach ($variations as $variation) {
                                        $sizeStr = $variation['attributes']['attribute_pa_size'];
                                        $sizeStr = wc_get_formatted_variation( ['attribute_pa_size' => $sizeStr ], true, false);

                                        if (!in_array($sizeStr, $supersizeRange)) {
                                            break;
                                        }
                                        $size = floatval(preg_replace('/[^0-9\.]/', '', $sizeStr));
                                        $sizeUnit = preg_replace('/[0-9\.]+/', '', $sizeStr);
                                        if ((strtoupper($currentSizeUnit) === 'G' || strtoupper($currentSizeUnit) === 'GS') && (strtoupper($sizeUnit) === 'KG' || strtoupper($sizeUnit) === 'KGS')) {
                                            $size = $size * 1000;
                                        } elseif ((strtoupper($currentSizeUnit) === 'KG' || strtoupper($currentSizeUnit) === 'KGS') && (strtoupper($sizeUnit) === 'G' || strtoupper($sizeUnit) === 'GS')) {
                                            $currentSize = $currentSize * 1000;
                                        }
                                        $flavour = $variation['attributes']['attribute_pa_flavour'];
                                        // Considering the case there are 3 sizes: 2lbs, 5lbs, 10lbs and customers buy 2lbs. Supersize suggestion should be 5lbs not 10bls.
                                        if ($size > $currentSize && ($nextSize === 0 || $size < $nextSize) && $flavour === $currentFlavour) {
                                            $nextSizeStr = $sizeStr;
                                            $nextSize = $size;
                                            $suggestion = $variation;
                                        }
                                    }
                                    // check stock
                                    if (!empty($suggestion)) {
                                        $suggestedVariation = new WC_Product_Variation($suggestion['variation_id']);
                                        $stock = $suggestedVariation->get_stock_quantity();
                                        // if current cart item is free (BOGO discount rule could result in the cheapest product in cart is free) then hidden supersize option
                                        if ($stock >= $cart_item['quantity'] && $cart_item['line_subtotal'] > 0) {
                                            $currentVariationId = $_product->variation_id;
                                            $currentUnitPrice = $_product->get_sale_price() / $currentSize;

                                            $suggestedVariationId = $suggestedVariation->variation_id;
                                            $suggestionUnitPrice = $suggestedVariation->get_sale_price() / $nextSize;
                                            if ((strtoupper($currentSizeUnit) === 'G' || strtoupper($currentSizeUnit) === 'GS') && (strtoupper($sizeUnit === 'KG' || strtoupper($sizeUnit === 'KGS')))) {
                                                $suggestionUnitPrice = $suggestionUnitPrice * 1000;
                                            }
                                            $saveMoney = ($currentUnitPrice - $suggestionUnitPrice) * $nextSize;
                                    ?>
                                    <tr>
                                        <td colspan="6" style="border-top: none; text-align: center">
                                            <a href="<?php echo '?supersize=1&from=' . $currentVariationId . '&to=' . $suggestedVariationId . '&qty=' . $cart_item['quantity'] ?>"
                                                style="border: 1px solid; border-radius: 20px; padding: 0.3rem 0.5rem; margin-right: 1rem;">Supersize Now</a>
                                            <span>Supersize to <?php
                                                echo $nextSizeStr . ': save ' . wc_price($currentUnitPrice - $suggestionUnitPrice) . ' per ' . (strtoupper($currentSizeUnit) == 'LBS' ? 'lb' : $currentSizeUnit) . ' = ' . wc_price($saveMoney) . ' savings'
                                            ?></span>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                }
                            }
                        }
                        // end of supersize function
                    }
                }
                ?>

                <?php do_action('woocommerce_cart_contents'); ?>
                <?php do_action('woocommerce_after_cart_contents'); ?>
            </tbody>
        </table>

        <?php do_action('woocommerce_before_actions'); ?>

        <div class="actions">
            <?php if (wc_coupons_enabled()) { ?>
                <div class="coupon">
                    <label for="coupon_code" class="visually-hidden"><?php esc_html_e('Coupon:', 'woocommerce'); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e('Coupon code', 'woocommerce'); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>"><?php esc_attr_e('Apply coupon', 'woocommerce'); ?></button>
                    <?php do_action('woocommerce_cart_coupon'); ?>
                </div>
            <?php } ?>

            <button type="submit" class="button" name="update_cart" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>

            <?php do_action('woocommerce_cart_actions'); ?>

            <?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
        </div>

        <?php do_action('woocommerce_after_cart_table'); ?>
    </form>

    <?php do_action('woocommerce_before_cart_collaterals'); ?>

    <div class="cart-collaterals">
        <?php
            /**
             * Cart collaterals hook.
             *
             * @hooked woocommerce_cross_sell_display
             * @hooked woocommerce_cart_totals - 10
             */
            do_action('woocommerce_cart_collaterals');
        ?>
    </div>
</div>

<?php do_action('woocommerce_after_cart'); ?>
