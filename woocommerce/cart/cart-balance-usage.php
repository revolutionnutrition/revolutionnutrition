<?php
if (wc_gc_is_redeeming_enabled() && $totals[ 'available_total' ] > 0 && $has_balance && apply_filters('woocommerce_gc_checkout_show_balance_checkbox', true)) :
    ?>
    <div class="update_totals_on_change">
        <label for="use_gift_card_balance">
            <input type="checkbox" id="use_gift_card_balance" name="use_gift_card_balance"<?php echo $use_balance ? ' checked' : ''; ?>>
            <?php echo sprintf(__('Use <strong>%s</strong> from your Gift Card balance.', 'woocommerce-gift-cards'), wp_kses_post(wc_price($totals[ 'available_total' ]))); ?>
            <?php if ($totals[ 'pending_total' ] > 0) { ?>
                <small class="woocommerce-MyAccount-Giftcards-pending-amount">
                    <?php
                    $link_text = esc_html__('pending orders', 'woocommerce-gift-cards');
                    $link      = add_query_arg(array( 'wc_gc_show_pending_orders' => 'yes'), wc_get_account_endpoint_url('orders'));
                    /* translators: %1$s: text link, %2$s pending amount */
                    echo sprintf(__(' %2$s on hold in %1$s', 'woocommerce-gift-cards'), '<a href="' . $link . '">' . $link_text . '</a>', wc_price($totals[ 'pending_total' ])); ?>
                    <span class="warning-icon"></span>
                </small>
            <?php } ?>
        </label>
    </div>
    <?php
endif;
