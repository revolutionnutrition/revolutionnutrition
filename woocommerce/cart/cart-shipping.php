<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

$formatted_destination = isset($formatted_destination) ? $formatted_destination : WC()->countries->get_formatted_address($package['destination'], ', ');
$has_calculated_shipping = !empty($has_calculated_shipping);
$show_shipping_calculator = !empty($show_shipping_calculator);
$calculator_text = '';

?>
<div class="woocommerce-shipping-totals">
    <h3><?php _e('Shipping Method', DOMAIN); ?></h3>
    <div data-title="<?php echo esc_attr($package_name); ?>" class="shipping-options<?php echo $available_methods ? '' : ' empty'; ?>">
        <?php if ($available_methods) :
            $firstMethod = array_values($available_methods)[0];
            $estDelivery = '';
            $closestEstimate = null;
            $accepted = [
                'flat_rate',
                'free_shipping',
            ];

            $checkFirst = null;

            if (in_array($firstMethod->get_method_id(), $accepted)) :
                if (array_key_exists('wf_shipping_canada_post:DOM.EP', $available_methods)) :
                    $checkFirst = $available_methods['wf_shipping_canada_post:DOM.EP'];
                endif;

                if (array_key_exists('wf_shipping_canada_post:USA.EP', $available_methods)) :
                    $checkFirst = $available_methods['wf_shipping_canada_post:USA.EP'];
                endif;

                if (!is_null($checkFirst)) :
                    $meta = $checkFirst->get_meta_data();

                    if (array_key_exists('phive_shipping_method', $meta)) :
                        $estDelivery = $meta['phive_shipping_method']['est_delivery'];
                    endif;
                endif;

                if (empty($estDelivery)) :
                    foreach ($available_methods as $method) :
                        $meta = $method->get_meta_data();

                        if (!array_key_exists('phive_shipping_method', $meta)) :
                            continue;
                        endif;

                        $firstDate = explode(' - ', $meta['phive_shipping_method']['est_delivery']);
                        $firstDate = new DateTime($firstDate[0]);

                        if (is_null($closestEstimate) || $firstDate > $closestEstimate) :
                            $closestEstimate = $firstDate;
                            $estDelivery = $meta['phive_shipping_method']['est_delivery'];
                        endif;
                    endforeach;
                endif;
            endif;

            usort($available_methods, function ($method1, $method2) {
                return floatval($method1->get_cost()) > floatval($method2->get_cost());
            });

            ?>
            <ul id="shipping_method" class="woocommerce-shipping-methods">
                <?php foreach ($available_methods as $method) : ?>
                    <li>
                        <?php
                        if (1 < count($available_methods)) {
                            printf('<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />', $index, esc_attr(sanitize_title($method->id)), esc_attr($method->id), checked($method->id, $chosen_method, false));
                        } else {
                            printf('<input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" />', $index, esc_attr(sanitize_title($method->id)), esc_attr($method->id));
                        }

                        ?>
                        <label for="shipping_method_<?php echo $index; ?>_<?php echo esc_attr(sanitize_title($method->id)); ?>">
                            <?php echo wc_cart_totals_shipping_method_label($method); ?>
                            <?php if ($firstMethod->get_id() === $method->get_id() && !empty($estDelivery)) : ?>
                                <br><small><?php _e('Est delivery: ', DOMAIN); ?><?php echo $estDelivery; ?></small>
                            <?php endif; ?>
                        </label>
                        <?php do_action('woocommerce_after_shipping_rate', $method, $index); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?php
        elseif (!$has_calculated_shipping || !$formatted_destination) :
            _e('Enter your shipping address to see shipping options', DOMAIN);
        elseif (!is_cart()) :
            echo wp_kses_post(apply_filters('woocommerce_no_shipping_available_html', __('There are no shipping options available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce')));
        else :
            echo wp_kses_post(apply_filters('woocommerce_cart_no_shipping_available_html', sprintf(esc_html__('No shipping options were found for %s.', 'woocommerce') . ' ', '<strong>' . esc_html($formatted_destination) . '</strong>')));
            $calculator_text = esc_html__('Enter a different address', 'woocommerce');
        endif;
        ?>

        <?php if ($show_package_details) : ?>
            <?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html($package_details) . '</small></p>'; ?>
        <?php endif; ?>

        <?php if ($show_shipping_calculator) : ?>
            <?php woocommerce_shipping_calculator($calculator_text); ?>
        <?php endif; ?>
    </div>
</div>
