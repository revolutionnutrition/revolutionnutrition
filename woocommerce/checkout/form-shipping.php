<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined('ABSPATH') || exit;
?>
<div class="woocommerce-shipping-fields">
    <h3><?php _e('Shipping Address', DOMAIN); ?></h3>

    <?php if (true === WC()->cart->needs_shipping_address()) : ?>
        <div class="shipping-address-toggle">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox" id="use-same-address">
                <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" type="radio" name="ship_to_different_address" value="0" checked="checked" /> <span><?php esc_html_e('Same as billing address', DOMAIN); ?></span>
            </label>
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox" id="ship-to-different-address">
                <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" type="radio" name="ship_to_different_address" value="1" /> <span><?php esc_html_e('Use a different address', DOMAIN); ?></span>
            </label>
        </div>

        <div class="shipping_address">
            <?php do_action('woocommerce_before_checkout_shipping_form', $checkout); ?>

            <div class="woocommerce-shipping-fields__field-wrapper">
                <?php $fields = $checkout->get_checkout_fields('shipping'); ?>
                <?php foreach ($fields as $key => $field) : ?>
                    <?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
                <?php endforeach; ?>
            </div>

            <?php do_action('woocommerce_after_checkout_shipping_form', $checkout); ?>
        </div>
    <?php endif; ?>
</div>
