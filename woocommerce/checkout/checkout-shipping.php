<?php

defined('ABSPATH') || exit;

if ($available_methods) : ?>
    <?php foreach ($available_methods as $method) : ?>
        <?php if ($method->id === $chosen_method) : ?>
            <tr class="woocommerce-shipping-price shipping">
                <th colspan="2"><?php echo wp_kses_post($package_name); ?> - <?php echo $method->get_label(); ?></th>
                <td data-title="<?php echo esc_attr($package_name); ?>">
                    <?php
                    if (WC()->cart->display_prices_including_tax()) {
                        $label = wc_price($method->cost + $method->get_shipping_tax());

                        if ($method->get_shipping_tax() > 0 && ! wc_prices_include_tax()) {
                            $label .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
                        }
                    } else {
                        $label = wc_price($method->cost);

                        if ($method->get_shipping_tax() > 0 && wc_prices_include_tax()) {
                            $label .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
                        }
                    }

                    echo $label;
                    ?>
                </td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
