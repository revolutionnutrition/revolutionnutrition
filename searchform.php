<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
    <input type="hidden" name="post_type" value="product" />
    <button type="button" class="nav-button" aria-label="Toggle search field">
        <em class="far fa-<?php echo !empty($_GET['s']) ? 'times' : 'search'; ?>"></em>
    </button>

    <div class="search-popout<?php echo !empty($_GET['s']) ? ' visible' : ''; ?>">
        <label for="s" class="screen-reader-text"><?php _e('Search for:', DOMAIN); ?></label>
        <input type="search" id="s" name="s" value="<?php echo !empty($_GET['s']) ? get_search_query() : ''; ?>">
        <button type="submit" class="form-button" aria-label="Search">
            <em class="far fa-search" aria-hidden="true"></em>
        </button>
    </div>
</form>
