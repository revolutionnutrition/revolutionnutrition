<div class="<?php Layout::classes('promo'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <a href="<?php Field::display('link.url'); ?>" class="desktop-image"><?php Field::image('desktop', 'banner'); ?></a>
    <a href="<?php Field::display('link.url'); ?>" class="mobile-image"><?php Field::image('mobile', 'banner'); ?></a>
</div>
