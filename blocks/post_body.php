<div class="content-with-sidebar side-right">
    <div class="container">
        <div class="content">
            <div class="block-title">
                <h2 class="title-normal"><?php the_title(); ?></h2>
            </div>
            <?php the_content(); ?>
        </div>
        <aside class="sidebar">
            <div class="block-title">
                <h3 class="title-normal"><?php _e('Related News', DOMAIN); ?></h3>
            </div>
            <?php

            $articles = new WP_Query([
                'posts_per_page' => 8,
            ]);

            if ($articles->have_posts()) :
                echo '<ul>';

                while ($articles->have_posts()) : $articles->the_post();
                    echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
                endwhile;

                echo '</ul>';
            endif;

            ?>
        </aside>
    </div>
</div>
