<div class="<?php Layout::classes('columns'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php if (Field::exists('columns')) : ?>
            <div class="inner">
                <?php foreach (Field::iterable('columns') as $loop) : ?>
                    <div class="col">
                        <?php Layout::flexible(Field::get('content', []), 'components'); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
