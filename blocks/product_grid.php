<div class="<?php Layout::classes('product-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php if (Field::exists('products')) : ?>
            <?php echo do_shortcode('[products limit="6" columns="3" ids="' . implode(',', Field::get('products')) . '"]');?>
        <?php endif; ?>
    </div>
</div>
