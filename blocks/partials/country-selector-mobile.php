<?php
    
$serverName = $_SERVER['SERVER_NAME'];

if (strcasecmp($serverName, 'www.revolution-nutrition.com') == 0) : ?>
    <div class="country-selector-wrapper country-selector-wrapper-mobile">
        <label id="country-selector-mobile">
            <div class="country-selector-button"><img class="country-flag" src="<?php echo get_template_directory_uri(); ?>/img/canada-flag.png" alt="Canada">CA</div>
        </label>

        <div id="country-selector-content-mobile" class="country-selector-content">
            <a href="https://www.us.revolution-nutrition.com"><img class="country-flag" src="<?php echo get_template_directory_uri(); ?>/img/united-states-flag.png" alt="United States">US</a>
        </div>
    </div>
<?php elseif (strcasecmp($serverName, 'www.us.revolution-nutrition.com') == 0) : ?>
    <div class="country-selector-wrapper country-selector-wrapper-mobile">
        <label id="country-selector-mobile">
            <div class="country-selector-button"><img class="country-flag" src="<?php echo get_template_directory_uri(); ?>/img/united-states-flag.png" alt="United States">US</div>
        </label>

        <div id="country-selector-content-mobile" class="country-selector-content">
            <a href="https://www.revolution-nutrition.com"><img class="country-flag" src="<?php echo get_template_directory_uri(); ?>/img/canada-flag.png" alt="Canada">CA</a>
        </div>
    </div>
<?php endif;