<?php if (get_field('announcements', 'options')) : ?>
    <div class="announcement-bar"<?php echo get_field('announcement_bar_colour', 'option') ? ' style="background-color: ' . get_field('announcement_bar_colour', 'option') . '"' : ''; ?><?php echo get_field('transition_speed', 'option') ? ' data-speed="' . intval(get_field('transition_speed', 'option') * 1000) . '"' : ''; ?>>
        <?php foreach (get_field('announcements', 'options') as $announcement) :
            $styles = [];
            $scheduled = new ScheduledItem($announcement);

            if (!$scheduled->isVisible()) :
                continue;
            endif;

            if (array_key_exists('color', $announcement) && !empty($announcement['color'])) :
                $styles[] = 'color: ' . $announcement['color'];
            endif;

            if (array_key_exists('background_colour', $announcement) && !empty($announcement['background_colour'])) :
                $styles[] = 'background-color: ' . $announcement['background_colour'];
            endif;

            ?>

            <div class="announcement"<?php echo !empty($styles) ? ' style="' . implode('; ', $styles) . '"' : ''; ?>>
                <div class="container">
                    <?php if ($announcement['link']) : ?>
                        <a href="<?php echo $announcement['link']['url']; ?>"><?php echo $announcement['text']; ?></a>
                    <?php else : ?>
                        <?php echo $announcement['text']; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
