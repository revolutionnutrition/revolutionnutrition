<?php
    
$country = array_key_exists('HTTP_CF_IPCOUNTRY', $_SERVER) ? $_SERVER['HTTP_CF_IPCOUNTRY'] : find_country_based_on_remote_ip($_SERVER['REMOTE_ADDR']);
$hideCountry = $_COOKIE['hidecountryheader'];
$serverName = $_SERVER['SERVER_NAME'];

if (strcasecmp($country, 'US') == 0 && strcasecmp($serverName, 'www.revolution-nutrition.com') == 0 && strcasecmp($hideCountry, 'true') != 0) : ?>
    <div id="country-popup-header" class="for-country-header">
        <div class="country-content-wrapper">
            <div class="country-head-text">It looks like you are in United States, would you like to visit our United States store?</div>
            <div class="country-button-wrapper">
                <a href="https://www.us.revolution-nutrition.com/" class="country-continue-button">Go ahead</a>
                <div id="country-cancel-button" class="country-cancel-button">&#215;</div>
            </div>
        </div>
    </div>
<?php elseif (strcasecmp($country, 'CA') == 0 && strcasecmp($serverName, 'www.us.revolution-nutrition.com') == 0 && strcasecmp($hideCountry, 'true') != 0) : ?>
    <div id="country-popup-header" class="for-country-header">
        <div class="country-content-wrapper">
            <div class="country-head-text">It looks like you are in Canada, would you like to visit our Canadian store?</div>
            <div class="country-button-wrapper">
                <a href="https://www.revolution-nutrition.com/" class="country-continue-button">Go ahead</a>
                <div id="country-cancel-button" class="country-cancel-button">&#215;</div>
            </div>
        </div>
    </div>
<?php endif;
