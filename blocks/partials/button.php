<?php

if (Field::exists('btn_label')) :
    $fields = [
        'class' => 'btn',
        'label' => '<span class="btn-label">' . Field::get('btn_label') . '</span>',
        'before' => '',
        'after' => '',
    ];

    if (Field::exists('btn_icon')) :
        $fields['before'] = '<em class="far fa-' . Field::get('btn_icon') . '"></em>';
        $fields['class'] .= ' icon-' . Field::get('btn_icon_placement');
    endif;

    Layout::partial('link', $fields);
endif;
