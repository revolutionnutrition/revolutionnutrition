<?php if (Field::anyExist('bg_video_webm', 'bg_video_mp4')) : ?>
    <div class="video-bg">
        <video playsinline autoplay muted loop>
            <?php Field::html('bg_video_webm', '<source src="%s" type="video/webm">'); ?>
            <?php Field::html('bg_video_mp4', '<source src="%s" type="video/mp4">'); ?>
        </video>
    </div>
<?php endif; ?>
