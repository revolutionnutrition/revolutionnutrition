<?php if (get_field('social_platforms', 'option')) : ?>
    <?php foreach (get_field('social_platforms', 'option') as $option) : ?>
        <a href="<?php echo $option['url']; ?>" target="_blank" rel="noopener" aria-label="Visit us at <?php echo $option['icon'] === 'facebook-f' ? 'facebook' : $option['icon']; ?>"><em class="fab fa-<?php echo $option['icon']; ?>" aria-hidden="true"></em></a>
    <?php endforeach; ?>
<?php endif; ?>
