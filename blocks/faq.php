<div class="<?php Layout::classes('faq'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php foreach (Field::iterable('groups') as $loop) : ?>
            <div class="faq-group">
                <?php Field::html('label', '<h3 class="faq-title">%s</h3>'); ?>
                <div class="faq-items">
                    <?php foreach (Field::iterable('questions') as $loop2) : ?>
                        <div class="faq-item<?php echo $loop->first && $loop2->first ? ' active' : ''; ?>">
                            <?php Field::html('question', '<p class="faq-handle">%s</p>'); ?>
                            <?php Field::html('answer', '<div class="faq-content">%s</div>'); ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
