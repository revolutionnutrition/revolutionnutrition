<div class="<?php Layout::classes('accent-image'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <div class="accent">
            <?php Field::image('accent', 'large'); ?>
        </div>
        <div class="content">
            <?php Layout::partial('title'); ?>
            <?php Field::html('content', '<div class="desc">%s</div>'); ?>
            <?php Layout::partial('buttons'); ?>
        </div>
    </div>
</div>
