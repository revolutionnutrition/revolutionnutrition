<div class="<?php Layout::classes('content-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php if (Field::exists('items')) : ?>
            <div class="items <?php Field::html('items_per_row', 'x%s', 'x3'); ?>">
                <?php foreach (Field::iterable('items') as $loop) : ?>
                    <div class="item">
                        <?php Field::image('image', 'medium', ['class' => 'thumbnail']); ?>
                        <?php Field::html('label', '<p class="label">%s</p>'); ?>
                        <?php Field::display('description'); ?>
                        <?php Layout::partial('button'); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
