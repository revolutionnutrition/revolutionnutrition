<div class="<?php Layout::classes('product-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php if (Field::exists('category')) : ?>
            <?php echo do_shortcode('[product_category limit="' . Field::get('product_limit') . '" columns="3" category="' . implode(',', Field::get('category')) . '"]');?>
        <?php endif; ?>
    </div>
</div>
