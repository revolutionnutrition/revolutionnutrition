<?php

if (Field::exists('mobile_image')) :
    Layout::addClass('split-text');
endif;

$link = null;

if (Field::exists('buttons')) :
    $firstBtn = Field::get('buttons.0');
    $link = $firstBtn['link_type'] === 'url' ? $firstBtn['link'] : $firstBtn['link_file'];
endif;

?>
<div class="<?php Layout::classes('promo-layered'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <div class="container">
        <?php if (!is_null($link)) : ?>
            <a href="<?php echo $link['url']; ?>" class="inner">
        <?php else : ?>
            <div class="inner">
        <?php endif; ?>

        <?php foreach (Field::iterable('text') as $loop) : ?>
            <p class="<?php Field::stack(['size', 'color', 'weight'], ' '); ?>"<?php Field::displayIfEquals('color', 'text-custom', ' style="color: ' . Field::get('custom_color') . '"'); ?>>
                <?php if (Field::exists('spacer')) : ?>
                    &nbsp;
                <?php else : ?>
                    <?php Field::display('text'); ?>
                <?php endif; ?>
            </p>
        <?php endforeach; ?>

        <?php if (!is_null($link)) : ?>
            </a>
        <?php else : ?>
            </div>
        <?php endif; ?>


        <?php if (!is_null($link) && Field::exists('mobile_image')) : ?>
            <a href="<?php echo $link['url']; ?>" class="mobile-link">
                <?php Field::image('mobile_image'); ?>
            </a>
        <?php else : ?>
            <?php Field::image('mobile_image'); ?>
        <?php endif; ?>

        <?php Layout::partial('buttons'); ?>
    </div>
</div>
