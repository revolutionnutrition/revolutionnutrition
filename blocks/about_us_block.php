<div class="mainContainer">

  <div class="hwrap">
    <h1 class="title2">THE POWER</h1>
    <h1 class="titleof" id="titleofid">OF</h1>

    <h1 class="title3">
      <span class="wiglewrapper">
        <span id="I">I</span>
        <span id="N">N</span>
        <span id="N2">N</span>
        <span id="O">O</span>
        <span id="V">V</span>
        <span id="A">A</span>
        <span id="T">T</span>
        <span id="I2">I</span>
        <span id="O2">O</span>
        <span id="N3">N</span>
      </span>

    </h1>

  </div>


  <hr class="hr1_1" id="hr1">
  <div class="Maintxt1" id="Maintxt1idparent">

    <h1 class="Maintxt1head" id="Maintxt1id">Revolution Nutrition was founded in 2007 by a young team of health and technology enthusiasts. Revolution quickly became the industry standard for its unparalleled mainstream approach which brought immediate attention to the brand through a broad demographic of people, ranging from health and wellness enthusiasts to the general public community.
      <br> <br>From its inception, Revolution was profoundly built on research and development which has contributed to bringing to the market ‘’outside of the box’’ innovative concepts along with unmatchable formulations.</h1>
  </div>

  <div class="subcontainer1">

    <div class="forCover"id="forcoverid">

    </div>
    <h2 id="subtext1id"class="subtext1">Innovative And Intensive Research
      <hr style="border-top: 2px solid #eee;width:90%;margin-left:auto;margin-right:auto;">
      <p style="text-align: left;padding-right:10px;font-size: 17px;line-height: 1.5;font-family: 'Lato',sans-serif;">
        Revolution made the supplement world evolve very rapidly. With an extensive assortment of real dessert and candy flavored food and supplement products, replicating to the detail the utmost experience, Revolution Nutrition brought to the health community, the most delicious and innovative protein powders ever developed. In 2016 Revolution became the fastest growing sports nutrition brand in North America winning awards such as ‘’Brand of the year’’ and ‘’Most innovative sports nutrition Brand’’ at several big box stores across North America. Throughout the year of 2019, Revolution has evolved its business model into the most sophisticated e-commerce platform and became a fully integrated and automated B2C business model.
      </p>
    </h2>

  </div>

  <div class="columntest" id="columntestid">

    <article class="colunmnarticle">
      <div class="imagewrap">
        <img class="columnimag1"src="<?php echo THEME_IMG_PATH; ?>/iStock-1224202684_optimized.jpg" alt="certified" width="50%" height="100%">
      </div>

      <div class="columnpara1">
        <h1 class="columnhead">cGMP Facility</h1>
        <hr class="hrbase" id="hr2">
        Revolution Nutrition is uniquely positioned in the marketplace as a fully integrated ‘‘manufacture to manufacture’’ B2C e-commerce business model. Revolution Nutrition is one of the world's most popular sports nutrition brand and it owns a state of the art, Health Canada site licence, FDA registered, GMP compliant and Safe Food For Canadians License establishment facility. Revolution Nutrition does manufacture brands for some of the largest retailers in the world. </div>

  </article>
</div>








<div class="subcontainer2_2" id="subcontainer2_2id">
  <h1 class="columnhead">What Do We Offer</h1>
  <hr class="hr1" id="hr4">
  <div class="subslideshowwrapper">

    <div class="slidecontainer2">

      <div class="mySlides2" id="main-img" >

      </div>
      <div class="mySlides2" id="main-img3" >

      </div>
      <div class="mySlides2" id="main-img2" >

      </div>

      <!-- <img class="mySlides2" src="<?php echo THEME_IMG_PATH; ?>/IsoSplashNewFlavoursMob-newedited.jpeg"> -->
      <!-- <img class="mySlides2" src="<?php echo THEME_IMG_PATH; ?>/istockphoto-1306973369-1024x1024.jpg"  style="width:100%"> -->
      <!-- <img class="mySlides2" src="<?php echo THEME_IMG_PATH; ?>/istockphoto-1202574159-1024x1024.jpg"  style="width:100%"> -->
    </div>

    <div class="buttonWrapper2">

    <button class="w3-button demo2" onclick="currentDiv2(1)">
      <h2>INCREDIBLE FLAVOUR SYSTEM</h2>
      The Revolution Nutrition™ Research and Development team is also constantly working on flavour enhancement. When you choose to purchase any Revolution Nutrition™ product, you can be assured that you are getting a supplement that will not only deliver on the results you are expecting, but that you will also experience incredible mouth-watering flavours- Guaranteed!
    </button>
    <button class="w3-button demo2" onclick="currentDiv2(2)">
      <h2>INNOVATION</h2>
      Revolution Nutrition™ is committed to bringing you the most innovative and effective sports nutrition products on the market today. Science and innovation are always at the core of our product development.
    </button>
    <button class="w3-button demo2" onclick="currentDiv2(3)">
      <h2>SCIENCE</h2>
      Our research and development team constantly works at creating the most innovative formulas possible in order to supply our customers with the best and most effective products. Our team works with the latest ingredients from around the world to create the most potent formulations to ensure maximum results.
    </button>

    <div id="dotwrapper">
      <span class="dot2"></span>
      <span class="dot2"></span>
      <span class="dot2"></span>
    </div>


  </div>
</div>

</div>


<div class="subcontainer2" id="subcontainer2id">
<h1 class="columnhead">VERIFIED QUALITY</h1>
<hr class="hr1" id="hr3">

<div class="subcontainer2Wrapper">


  <div class="forCover2"id="forcoverid2">
    <img class="" src="<?php echo THEME_IMG_PATH; ?>/iStock-1200230502_optimized.jpg"  style="width:100%">


</div>
<div class="subtext2" id="subtext2id">
<!-- <h1 class="columnhead2">High Standard Products</h1>
<hr class="hrbase" id="hr5"> -->
Our products are manufactured in our own Good Manufacturing Practices (GMP) certified facility to ensure the highest quality standards. Each and every lot of all products are tested by our Quality Assurance department and by a third-party laboratory to ensure maximum quality control. We take pride in offering the highest standards to our customers.
</div>

</div>
</div>







<div class="hovermaincontainer" id="hovermaincontainerid">


<a href="https://www.revolution-nutrition.com/product-category/all-products/" class="card credentialing">



  <div class="overlay"></div>
  <div class="circle">


<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" version="1.0" width="100%" viewBox="0 0 548 548" preserveAspectRatio="xMidYMid meet" id="svg6" sodipodi:docname="image2.svg" inkscape:version="1.0.1 (3bc2e813f5, 2020-09-07)">
<metadata id="metadata12">
  <rdf:RDF>
    <cc:Work rdf:about="">
      <dc:format>image/svg+xml</dc:format>
      <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
    </cc:Work>
  </rdf:RDF>
</metadata>
<defs id="defs10"/>
<sodipodi:namedview pagecolor="#ffffff" bordercolor="#666666" borderopacity="1" objecttolerance="10" gridtolerance="10" guidetolerance="10" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-width="1920" inkscape:window-height="1001" id="namedview8" showgrid="false" inkscape:zoom="1.5145985" inkscape:cx="339.44683" inkscape:cy="318.57609" inkscape:window-x="-9" inkscape:window-y="-9" inkscape:window-maximized="1" inkscape:current-layer="g4"/>
<g fill-rule="evenodd" id="g4" style="&#10;">
  <path id="animaterevo"d="m 277.10361,383.66024 c -23.6,-2.6 -44.9,-13.4 -58.9,-29.9 -5,-5.9 -12.2,-18.5 -13.6,-24 -0.5,-1.9 -0.8,-1.9 -6.4,-0.6 -8.4,2 -8.7,1.7 -1.3,-1.4 3.5,-1.6 6.4,-3.3 6.2,-3.9 -0.1,-0.6 -0.5,-4.7 -0.9,-9.1 -0.7,-9.4 0.3,-14.4 5.3,-24.4 1.8,-3.7 3.4,-7.2 3.4,-7.7 0,-1.3 -18.7,-10.3 -26.3,-12.6 l -5.8,-1.8 -3.6,3.4 c -4.6,4.5 -10.9,13.7 -13.9,20.4 -3.8,8.6 -5.6,17.2 -6.5,30.1 l -0.8,12 0.3,-15 c 0.4,-18.6 2.4,-26.8 10,-42 3.8,-7.5 5.1,-11.2 4.3,-11.7 -1.7,-1.1 -26.9,-5.7 -40.2,-7.3 -6.6,-0.8 -23.7,-2.1 -37.999996,-2.8 -29.4,-1.4 -27.6,-2 9.999996,-3.3 21.8,-0.8 48.7,0.4 68.4,3 l 10.5,1.4 3.8,-3.4 c 10.2,-9.2 22.2,-14.7 36.7,-16.6 7,-0.9 10.1,-0.9 14.5,0.1 9.3,2 19.4,7.7 26.3,14.7 3.4,3.5 6.6,6.3 7,6.3 2.5,0 12.8,-5.1 13.4,-6.6 0.4,-1 0.7,-4.6 0.8,-8.1 0.2,-16.6 -13,-35.3 -32,-45.2 -9.8,-5.1 -32.03809,-7.76657 -45.23809,-8.86657 l -19.70379,-1.73372 34.04188,1.10029 c 27.4,0.7 47.4,9.2 64.2,27.4 6.7,7.3 13.6,19.1 15.7,26.9 0.9,3.3 1.5,4.1 2.9,3.7 47.4,-14 138.5,-19.5 201.7,-12.1 l 11,1.3 -18.5,0.1 c -106.6,0.8 -194.9,23.7 -210.7,54.6 -2.2,4.1 -2.5,5.9 -2.2,11.6 0.3,5.7 0.9,7.5 4.2,12.3 5.9,9 19.9,18.2 39.1,25.8 9.4,3.7 7.2,3.5 -11.4,-0.7 -26.9,-6.3 -50.6,-15.1 -63.7,-23.8 l -5.1,-3.4 -3.8,2.3 c -2.1,1.3 -7.5,3.8 -12.1,5.6 -9.5,3.7 -10,4.5 -8.3,14.2 2.8,15.5 14.9,31 30.9,39.5 11.3,6.1 20.3,8.4 35.6,9.1 7.7,0.4 11.1,0.8 7.7,0.9 -3.4,0.1 -7.1,0.4 -8.2,0.7 -1.1,0.2 -6,0 -10.8,-0.5" id="path2" style="&#10;    fill: white;&#10;" sodipodi:nodetypes="ccccccccsccccccccccccccccccssccccccccccccsccccccccccscc"/>
</g>
</svg>

  </div>
  <p>SHOP</p>

</a>






<a href="https://www.revolution-nutrition.com/revolution-rewards-program/" class="card education">
  <div class="overlay"></div>
  <div class="circle">



  <img src="<?php echo THEME_IMG_PATH; ?>/fitcoin-logo-simplify-256.svg"  style="width:80%;z-index:101;">

  </div>
  <p>Fitcoin</p>
</a>









<a href="https://www.revolution-nutrition.com/my-account/" class="card wallet">
  <div class="overlay"></div>
  <div class="circle">


    <svg width="78px" height="60px" viewBox="0 0 496 471.96" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <!-- Generator: Sketch 42 (36781) - http://www.bohemiancoding.com/sketch -->
      <desc>Created with Sketch.</desc>
      <defs></defs>

      <svg
      version="1.1"
      viewBox="0 0 496 471.96"
      id="svg12"
      width="100%"
      height="100%">
      <path
      fill="#ffffff"
      d="m 472,295.96 h -8 v -104 a 8,8 0 0 0 -8,-8 h -24 v -72 a 8,8 0 0 0 -8,-8 H 318.54 a 88,88 0 1 0 -173.08,0 H 48 a 48.051,48.051 0 0 0 -48,48 v 272 a 48.051,48.051 0 0 0 48,48 h 408 a 8,8 0 0 0 8,-8 v -104 h 8 a 24.032,24.032 0 0 0 24,-24 v -16 a 24.032,24.032 0 0 0 -24,-24 z m -240,-280 a 72,72 0 1 1 -72,72 72.083,72.083 0 0 1 72,-72 z m -184,104 h 102.02 a 88.006,88.006 0 0 0 163.96,0 H 416 v 64 H 48 a 32,32 0 0 1 0,-64 z m 400,336 H 48 a 32.03,32.03 0 0 1 -32,-32 V 187.7 a 47.8,47.8 0 0 0 32,12.26 h 400 v 96 h -24 a 8,8 0 0 0 -8,8 v 48 a 8,8 0 0 0 8,8 h 24 z m 32,-120 a 8.011,8.011 0 0 1 -8,8 h -40 v -32 h 40 a 8.011,8.011 0 0 1 8,8 z"
      id="path6_6" />
      <path
      fill="#ffffff"
      d="m 312,383.96 a 8,8 0 0 0 -8,8 v 40 a 8,8 0 0 0 8,8 h 112 a 8,8 0 0 0 8,-8 v -40 a 8,8 0 0 0 -8,-8 z m 104,40 h -96 v -24 h 96 z"
      id="path8_8" />
      <path
      fill="white"
      d="m 217.49952,33.663848 0.5618,111.979732 c 15.56346,-0.63809 15.56346,-0.63809 0,0 h -13.48335 v -59.0073 l -0.5618,-54.704351 h 64.04524 v 13.243983 h -63.48332 l 13.9739,25.607931 h 49.50942 v 12.84329 l -50.63326,0.25233 -12.85006,2.756817 v 59.0073 h 14.04528"
      style="fill:none;stroke:#ffffff;stroke-width:6.89765"
      id="path10_10"
      sodipodi:nodetypes="cccccccccccccccc" />
    </svg>

  </svg>

</div>
<p>E-wallet</p>
</a>










<a href="https://www.revolution-nutrition.com/contact/" class="card human-resources">
<div class="overlay"></div>
<div class="circle">


  <svg width="66px" height="77px" viewBox="1855 26 66 77" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(1855.000000, 26.000000)">
      <path d="M4.28872448,42.7464904 C4.28872448,39.3309774 5.4159227,33.7621426 6.40576697,30.4912557 C10.5920767,32.1098991 14.3021264,35.1207513 18.69596,35.1207513 C30.993618,35.1207513 42.5761396,28.7162991 49.9992251,17.9014817 C56.8027248,23.8881252 60.8188351,33.0463165 60.8188351,42.7464904 C60.8188351,60.817447 47.6104607,76.6693426 32.5537798,76.6693426 C17.4970989,76.6693426 4.28872448,60.817447 4.28872448,42.7464904" id="Fill-8" fill="#AFCEFF"></path>
      <path d="M64.3368879,31.1832696 L62.8424171,46.6027478 L60.6432609,46.7824348 L59.8340669,34.6791304 L47.6573402,25.3339478 C44.2906753,34.068487 34.3459503,40.2903304 24.4684093,40.2903304 C17.7559812,40.2903304 10.046244,37.4168 5.80469412,32.8004522 L5.80469412,34.6791304 L5.80469412,46.6027478 L4.28932167,46.6027478 L1.30187314,27.8802435 C1.30187314,20.9790957 3.52342407,15.5432 7.27229127,11.3578087 C13.132229,4.79558261 21.8124018,0.0492173913 30.5672235,0.342852174 C37.4603019,0.569286957 42.6678084,2.72991304 50.8299179,0.342852174 C51.4629405,1.44434783 51.8615656,3.00455652 51.5868577,5.22507826 C51.4629405,6.88316522 51.2106273,7.52302609 50.8299179,8.45067826 C58.685967,14.1977391 64.3368879,20.7073739 64.3368879,31.1832696" id="Fill-10" fill="#3B6CB7"></path>
      <path d="M58.9405197,54.5582052 C62.0742801,54.8270052 65.3603242,52.60064 65.6350321,49.5386574 C65.772386,48.009127 65.2617876,46.5570226 64.3182257,45.4584487 C63.3761567,44.3613357 62.0205329,43.6162922 60.4529062,43.4818922 L58.9405197,54.5582052 Z" id="Fill-13" fill="#568ADC"></path>
      <path d="M6.32350389,54.675367 C3.18227865,54.8492104 0.484467804,52.4957496 0.306803449,49.4264626 C0.217224782,47.8925496 0.775598471,46.4579757 1.75200594,45.3886191 C2.7284134,44.3192626 4.10792487,43.6165843 5.67853749,43.530393 L6.32350389,54.675367 Z" id="Fill-15" fill="#568ADC"></path>
    </g>
  </svg>

</div>
<p>Contact Us</p>
</a>


</div>


<!-- hover card ends -->



</div>
