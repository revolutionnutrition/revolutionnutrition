import SmoothScroll from './smoothscroll';

/**
 * Activate smooth scroll
 */
const scroll = new SmoothScroll();

/**
 * Add anchor link tracking
 */
const links = document.querySelectorAll('a[href*="#"]');

Array.from(links).forEach((element) => {
    let href = element.getAttribute('href');

    // Clean up URL
    if (href.indexOf('#') !== false) {
        href = href.substr(href.indexOf('#'));
    }

    if (href.length > 2) {
        element.addEventListener('click', (e) => {
            // Make sure the anchor is on the current page
            if (location.pathname.replace(/^\//, '') === element.pathname.replace(/^\//, '')
                && location.hostname === element.hostname) {
                e.preventDefault();
                scroll.to(href);
            }
        });
    }
});

/**
 * Check URL for hash
 */
window.addEventListener('load', () => {
    if (window.location.hash) {
        scroll.to(window.location.hash);
    }
});

/**
 * Search
 */
const searchButton = document.querySelector('.nav-button');
const searchPopout = document.querySelector('.search-popout');

if (searchButton) {
    searchButton.addEventListener('click', (e) => {
        e.preventDefault();

        searchPopout.classList.toggle('visible');
        searchButton.querySelector('.far').classList.toggle('fa-search');
        searchButton.querySelector('.far').classList.toggle('fa-times');
    });
}

/**
 * Megamenu
 */
const megaMenuItem = document.querySelectorAll('.menu-item');

Array.from(megaMenuItem).forEach((item) => {
    let megaEnterTimer;
    let megaExitTimer;
    let enterSubmenu;
    let exitSubmenu;

    item.addEventListener('mouseover', () => {
        enterSubmenu = item.querySelector('.sub-menu');
        clearTimeout(megaExitTimer);

        if (enterSubmenu) {
            megaEnterTimer = setTimeout(() => {
                enterSubmenu.classList.add('visible');
            }, 300);
        }
    });

    item.addEventListener('mouseout', () => {
        clearTimeout(megaEnterTimer);

        const innerMenu = item.querySelector('.sub-menu');

        if (innerMenu && innerMenu.classList.contains('visible')) {
            exitSubmenu = innerMenu;

            megaExitTimer = setTimeout(() => {
                exitSubmenu.classList.remove('visible');
            }, 400);
        }
    });
});

/**
 * Cookie Notice
 */
const noticeContainer = document.querySelector('.cookie-notice');

if (noticeContainer) {
    const noticeAcceptance = localStorage.getItem('notice_ignore');
    const noticeAgreement = noticeContainer.querySelector('.btn');

    if (!noticeAcceptance) {
        noticeContainer.classList.add('reveal');
    }

    noticeAgreement.addEventListener('click', () => {
        localStorage.setItem('notice_ignore', new Date());
        noticeContainer.classList.remove('reveal');
    });
}

/**
 * Mobile Navigation
 */
const mobileMenuButton = document.querySelector('.open-mobile-menu');
const mobileMenu = document.querySelector('.mobile-navigation');
const mobileCloseTarget = document.querySelector('.mobile-close-target');

if (mobileMenuButton) {
    mobileMenuButton.addEventListener('click', () => {
        mobileMenu.classList.toggle('open');
        mobileCloseTarget.classList.toggle('open');
    });
}

if (mobileCloseTarget) {
    mobileCloseTarget.addEventListener('click', () => {
        mobileMenu.classList.remove('open');
        mobileCloseTarget.classList.remove('open');
    });
}

/**
 * Product Description
 */
const categoryLearnMoreButtons = document.querySelectorAll('.desc-read-more');

Array.from(categoryLearnMoreButtons).forEach((button) => {
    button.addEventListener('click', () => {
        const inner = button.closest('.container').querySelector('.long-description .inner');
        const longDesc = button.closest('.container').querySelector('.long-description');

        if (longDesc.classList.contains('opened')) {
            longDesc.style.height = `${inner.clientHeight}px`;
            longDesc.classList.remove('opened');

            button.innerText = 'Read More';
            button.classList.remove('close');

            setTimeout(() => {
                longDesc.style.height = 0;
            }, 10);
        } else {
            longDesc.style.height = `${inner.clientHeight}px`;

            button.innerText = 'Close';
            button.classList.add('close');

            setTimeout(() => {
                longDesc.classList.add('opened');
                longDesc.style.height = 'auto';
            }, 310);
        }
    });
});

/**
 * Account Navigation
 */
const accountNavBtn = document.querySelector('.account-mobile-nav');
const accountMenu = document.querySelector('.account-nav-menu');

if (accountNavBtn) {
    accountNavBtn.addEventListener('click', (e) => {
        e.preventDefault();

        accountMenu.classList.toggle('open');
    });
}

/**
 * FAQ
 */
const faqToggle = document.querySelectorAll('.faq-handle');

Array.from(faqToggle).forEach((handle) => {
    handle.addEventListener('click', () => {
        const container = handle.closest('.container');
        const active = container.querySelector('.active');
        const content = handle.closest('.faq-item');

        if (active) {
            active.classList.remove('active');
        }

        content.classList.add('active');
    });
});

jQuery(function ($) {
    /**
     * Announcement
     */
    $('.announcement-bar').slick({
        dots: false,
        infinite: true,
        arrows: false,
        autoplay: true,
        fade: true,
        autoplaySpeed: $('.announcement-bar').data('speed') ? $('.announcement-bar').data('speed') : 2500,
    });

    /**
     * Incrementor
     */
    $('.woocommerce').on('click', '.quantity-incrementor .tick-up', function () {
        const updateCartBtn = document.querySelector('button[name="update_cart"]');
        const quantityInput = $(this).parent().find('.qty');

        quantityInput.val(parseInt(quantityInput.val(), 10) + 1);

        if (updateCartBtn) {
            updateCartBtn.disabled = false;
        }
    });

    $('.woocommerce').on('click', '.quantity-incrementor .tick-down', function () {
        const updateCartBtn = document.querySelector('button[name="update_cart"]');
        const quantityInput = $(this).parent().find('.qty');
        const value = parseInt(quantityInput.val(), 10);

        if (value > 1) {
            quantityInput.val(value - 1);

            if (updateCartBtn) {
                updateCartBtn.disabled = false;
            }
        }
    });

    $('.woocommerce').on('keyup', '.quantity-incrementor .qty', function () {
        const value = parseInt($(this).val(), 10);

        if (Number.isInteger(value)) {
            $(this).val(value);
        } else {
            $(this).val(1);
        }
    });

    /**
     * Catalog flavour selector
     */
    $(document).on('woocommerce_variation_has_changed', function (event) {
        const parent = $(event.target).closest('li.wvs-pro-product');
        const productURL = parent.find('a[data-product_permalink]').data('product_permalink');

        setTimeout(() => {
            const attribute = $(event.target).find('.image-variable-wrapper').data('attribute_name');
            const flavour = $(event.target).find('.image-variable-wrapper .selected').data('value');
            const newURL = `${productURL}?${attribute}=${flavour}`;

            parent.find('a.woocommerce-LoopProduct-link').attr('href', newURL);
            parent.find('a[data-product_permalink]').attr('href', newURL);
        }, 50);
    });

    /**
     * Shipping Address
     */
    $(document).on('change', '#use-same-address input', () => {
        $('div.shipping_address').hide();
    });

    /**
     * Promotion
     */
    $('body').on('click', '.woocommerce-message .code', function (e) {
        e.preventDefault();
        $('#coupon_code').val($(this).data("code"));
        $('.coupon .button').trigger("click");
    });

    /**
     * Fitcoin toggle switch for category page
     */
    const fitcoinToggleSwitch = $('#fitcoin-toggle-switch');

    fitcoinToggleSwitch.on('change', function () {
        if (fitcoinToggleSwitch.prop('checked') == true) {
            $('.fitcoin-earn').css('display', 'inline-block');
        } else {
            $('.fitcoin-earn').hide();
        }
    });

    /**
     * Country header
     */
    const countryHeader = $("#country-popup-header");
    if (countryHeader.length) {
 
     $('#main').addClass('main-content-with-country-selector');

    }
    const countryCancelButton = $('#country-cancel-button');
    countryCancelButton.on('click', function () {

        $('#main').removeClass("main-content-with-country-selector");

        $('#country-popup-header').hide();
        // expires cookie after 24 hours
        const dt = new Date();
        dt.setTime(dt.getTime() + (24*60*60*1000));
        let expires = "expires="+ dt.toGMTString();
        document.cookie = "hidecountryheader=true;" + expires + ";path=/";
    });

    /**
     * Country selector button
     */
     const primaryCountrySelectorButton = $('#country-selector-primary');
     primaryCountrySelectorButton.on('click', function (event) {
         event.stopPropagation();
         $('#country-selector-content-primary').toggleClass("show-country-selector");
     });
     const mobileCountrySelectorButton = $('#country-selector-mobile');
     mobileCountrySelectorButton.on('click', function (event) {
         event.stopPropagation();
         $('#country-selector-content-mobile').toggleClass("show-country-selector");
     });
    
    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches("#country-selector-primary")) {
            let dropdownPrimary = $("#country-selector-content-primary");
            if (dropdownPrimary.hasClass("show-country-selector")) {
                dropdownPrimary.removeClass("show-country-selector");
            }
        }

        if (!event.target.matches("#country-selector-mobile")) {
            let dropdownMobile = $("#country-selector-content-mobile");
            if (dropdownMobile.hasClass("show-country-selector")) {
                dropdownMobile.removeClass("show-country-selector");
            }
        }
    }

});

// Klaviyo checkbox = checked by default
const kl_newsletter = document.getElementById('kl_newsletter_checkbox');

if (kl_newsletter) {
    kl_newsletter.checked = true;
}

const kl_sms = document.getElementById('kl_sms_consent_checkbox');
if (kl_sms) {
    kl_sms.checked = true;
}